import 'package:flutter/material.dart';
import 'package:flutter_do_an/res/my_config.dart';
import 'package:shimmer/shimmer.dart';

class ShimmerLoading{
  static Widget shimmerHome(){
    return Container(
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children:[
            Shimmer.fromColors(
              baseColor: MyColor.background,
              highlightColor: Colors.white10,
              child: Container(
                width: 150,
                height: 20,
                margin: EdgeInsets.only(top:MySpace.marginL),
                decoration: BoxDecoration(
                    color: Colors.black,
                    borderRadius: BorderRadius.all(Radius.circular(10))
                ),
              ),
            ),
            Container(
              height: 250,
              margin: EdgeInsets.only(top: MySpace.marginL),
              child: Shimmer.fromColors(
                baseColor: MyColor.background,
                highlightColor: Colors.white10,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  shrinkWrap: true,
                  itemCount: 3,
                  itemBuilder:(context,index)=> Container(
                    width: 140,
                    height: 250,
                    margin: EdgeInsets.only(right: 10),
                    decoration: BoxDecoration(
                        color: Colors.black,
                        borderRadius: BorderRadius.all(Radius.circular(10))
                    ),
                  ) ,
                ),
              ),
            ),
            Shimmer.fromColors(
              baseColor: MyColor.background,
              highlightColor: Colors.white10,
              child: Container(
                width: 100,
                height: 20,
                margin: EdgeInsets.only(top: MySpace.marginL),
                decoration: BoxDecoration(
                    color: Colors.black,
                    borderRadius: BorderRadius.all(Radius.circular(10))
                ),
              ),
            ),
            Container(
              height: 150,
              margin: EdgeInsets.only(top: MySpace.marginL),
              child: Shimmer.fromColors(
                baseColor: MyColor.background,
                highlightColor: Colors.white10,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  shrinkWrap: true,
                  itemCount: 3,
                  itemBuilder:(context,index)=> Container(
                    width: 140,
                    height: 120,
                    margin: EdgeInsets.only(right: 10),
                    decoration: BoxDecoration(
                        color: Colors.black,
                        borderRadius: BorderRadius.all(Radius.circular(10))
                    ),
                  ) ,
                ),
              ),
            ),

            Shimmer.fromColors(
              baseColor: MyColor.background,
              highlightColor: Colors.white10,
              child: Container(
                width: 100,
                height: 20,
                margin: EdgeInsets.only(top: MySpace.marginL),
                decoration: BoxDecoration(
                    color: Colors.black,
                    borderRadius: BorderRadius.all(Radius.circular(10))
                ),
              ),
            ),
            Container(
              height: 250,
              margin: EdgeInsets.only(top: MySpace.marginL),
              child: Shimmer.fromColors(
                baseColor: MyColor.background,
                highlightColor: Colors.white10,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  shrinkWrap: true,
                  itemCount: 3,
                  itemBuilder:(context,index)=> Container(
                    width: 140,
                    height: 250,
                    margin: EdgeInsets.only(right: 10),
                    decoration: BoxDecoration(
                        color: Colors.black,
                        borderRadius: BorderRadius.all(Radius.circular(10))
                    ),
                  ) ,
                ),
              ),
            ),
          ]
      ),
    );
  }
  static Widget shimmerVD({double width = 170, double height = 250}){
    return Shimmer.fromColors(
      baseColor: MyColor.background,
      highlightColor: Colors.white10,
      child: Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
            color: Colors.black,
            borderRadius: BorderRadius.all(Radius.circular(10))
        ),
      ),
    );
  }
  static Widget searchItemShimmer(){
    return Shimmer.fromColors(
      baseColor: MyColor.background,
      highlightColor: Colors.white10,
      child: Container(
        margin: EdgeInsets.only(bottom: 15),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: 40,
              height: 40,
              margin: EdgeInsets.only(right: 10),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.black
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(width: 250,height: 15,color: Colors.black,),
                Container(margin:EdgeInsets.only(top: 5),width: 150,height: 15,color: Colors.black),
              ],
            )
          ],
        ),
      ),
    );
  }
  static Widget listCelebShimmer(){
    return Shimmer.fromColors(
      baseColor: MyColor.background,
      highlightColor: Colors.white10,
      child: GridView.builder(
        shrinkWrap: true,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            childAspectRatio: 2/3,
            crossAxisCount: 2,
            crossAxisSpacing: 20,
            mainAxisSpacing: 10
        ),
        itemCount: 6,
        itemBuilder: (context,index){
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(color: Colors.black,height: 200,),
              Container(margin:EdgeInsets.only(top: 10),color: Colors.black,width: 100,height: 10,),
              Container(margin:EdgeInsets.only(top: 5),color: Colors.black,width: 80,height: 10,),
            ],
          );
        }
      )
    );
  }
  static Widget chatShimmer(){
    return Shimmer.fromColors(
      baseColor: MyColor.background,
      highlightColor: Colors.white10,
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: 5,
        itemBuilder: (context,index){
          return Container(
            margin: EdgeInsets.only(bottom: 16,left: 16),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(height: 50,width: 50,decoration: BoxDecoration(color: Colors.black,borderRadius: BorderRadius.circular(25))),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Column(crossAxisAlignment:CrossAxisAlignment.start,children: [
                    Container(margin:EdgeInsets.only(top: 10),color: Colors.black,width: MySpace.fullWidth(context)-106,height: 10,),
                    Container(margin:EdgeInsets.only(top: 5),color: Colors.black,width: 100,height: 10,),
                  ],),
                )

              ],
            ),
          );
        }
      )
    );
  }
  static Widget notifyShimmer(){
    return Shimmer.fromColors(
      baseColor: MyColor.background,
      highlightColor: Colors.white10,
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: 5,
        itemBuilder: (context,index){
          return Container(
            margin: EdgeInsets.only(bottom: 16,left: 32),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(height: 30,width: 30,decoration: BoxDecoration(color: Colors.black,borderRadius: BorderRadius.circular(25))),
                Container(margin:EdgeInsets.only(left: 8),color: Colors.black,width: MySpace.fullWidth(context)-106,height: 20,)

              ],
            ),
          );
        }
      )
    );
  }
}