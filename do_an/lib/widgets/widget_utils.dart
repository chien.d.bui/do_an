

import 'package:flutter/material.dart';
import 'package:flutter_do_an/res/my_config.dart';

class WidgetUtils{
  TextStyle textStyleTitle(double size){
    return TextStyle(color: Colors.white, fontSize: size,fontWeight: FontWeight.bold);
  }
  TextStyle textStyleTitleGrey(double size){
    return TextStyle(color: Colors.grey, fontSize: size,fontWeight: FontWeight.bold);
  }
  TextStyle textStyleNormal(double size){
    return TextStyle(color: Colors.white, fontSize: size);
  }
  TextStyle textStyleNormalGrey(double size){
    return TextStyle(color: Colors.grey, fontSize: size);
  }
  TextStyle textStyleTitleRed(double size){
    return TextStyle(color: Colors.red, fontSize: size,fontWeight: FontWeight.bold);
  }
  TextStyle textStyleNormalRed(double size){
    return TextStyle(color: Colors.red, fontSize: size);
  }
  TextStyle textStyleTitleCustom(double size, {Color color = MyColor.content}){
    return TextStyle(color: color, fontSize: size,fontWeight: FontWeight.bold);
  }
  TextStyle textStyleNormalCustom(double size, {Color color = MyColor.content}){
    return TextStyle(color: color, fontSize: size, decoration: TextDecoration.none);
  }



}