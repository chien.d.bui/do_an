import 'package:flutter/material.dart';
import 'package:flutter_do_an/res/my_config.dart';
class EditTextUtils {
  TextFormField getCustomEditTextLogin(
      {String labelValue = "",
        String hintValue = "",
        TextEditingController controller,
        TextInputType keyboardType = TextInputType.text,
        bool obscureText,
        Widget icon,
        double size = 15,
        FocusNode focusNode,
      }) {
    TextFormField textFormField = TextFormField(
      keyboardType: keyboardType,
      style: TextStyle(color: Colors.white, fontSize: size,fontWeight: FontWeight.bold),
      controller: controller,
      obscureText: obscureText,
      focusNode: focusNode,
      autocorrect: false,
      cursorColor: MyColor.tertiary,
      decoration: InputDecoration(
        fillColor: Colors.white.withOpacity(0.05),
        filled: true,
        contentPadding: EdgeInsets.fromLTRB(16, 0, 0, 0),
        labelText: labelValue,
        hintText: hintValue,
        labelStyle: TextStyle(color: MyColor.content, fontSize: size),
        suffixIcon: icon,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
        focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(8.0),borderSide: BorderSide(color: MyColor.tertiary)),
      ),
    );
    return textFormField;
  }
  TextFormField getCustomEditTextMessages(
      {
        TextEditingController controller,
        String hintText,
        TextInputType keyboardType = TextInputType.text,
        TextStyle textStyle,
        void onChange(String text),
        FocusNode focusNode,
      }) {
    TextFormField textFormField = TextFormField(
      onChanged: onChange,
      focusNode: focusNode,
      keyboardType: keyboardType,
      style: textStyle,
      controller: controller,
      autocorrect: false,
      cursorColor: MyColor.tertiary,
      enableSuggestions: false,
      textCapitalization: TextCapitalization.sentences,
      // textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        focusedBorder:OutlineInputBorder(
          borderSide: const BorderSide(color: MyColor.content, width: 1.0),
          borderRadius: BorderRadius.circular(25.0),
        ),
        contentPadding: EdgeInsets.fromLTRB(16, 0, 0, 0),
          fillColor: Colors.white.withOpacity(0.1),
        filled: true,
        hintText: hintText,
        hintStyle: textStyle,
        labelStyle: textStyle,
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0))),
    );
    return textFormField;
  }
  TextFormField getCustomEditTextNormal(
      {
        void onChange(String text),
        TextEditingController controller,
        TextInputType keyboardType = TextInputType.text,
        TextStyle textStyle,
        String hintText,
        FocusNode focusNode,
      }) {
    TextFormField textFormField = TextFormField(
      focusNode: focusNode,
      keyboardType: keyboardType,
      style: textStyle,
      autocorrect: false,
      controller: controller,
      onChanged: onChange,
      decoration: InputDecoration(
        focusedBorder:OutlineInputBorder(
          borderSide: const BorderSide(color: MyColor.content, width: 1.0),
          borderRadius: BorderRadius.circular(10.0),
        ),
        contentPadding: EdgeInsets.fromLTRB(16, 0, 16, 0),
        fillColor: Colors.white.withOpacity(0.05),
        filled: true,
        hintText: hintText,
        hintStyle: textStyle,
        labelStyle: textStyle,
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0))),
    );
    return textFormField;
  }
  TextFormField getCustomEditNumber(
      {
        void onChange(String text),
        TextEditingController controller,
        TextInputType keyboardType = TextInputType.text,
        TextStyle textStyle,
        String hintText,
        FocusNode focusNode,
      }) {
    TextFormField textFormField = TextFormField(
      focusNode: focusNode,
      keyboardType: keyboardType,
      style: textStyle,
      autocorrect: false,
      controller: controller,
      onChanged: onChange,
      decoration: InputDecoration(
        focusedBorder:OutlineInputBorder(
          borderSide: const BorderSide(color: MyColor.content, width: 1.0),
          borderRadius: BorderRadius.circular(10.0),
        ),
        contentPadding: EdgeInsets.fromLTRB(16, 0, 16, 0),
        fillColor: Colors.white.withOpacity(0.05),
        filled: true,
        hintText: hintText,
        hintStyle: textStyle,
        labelStyle: textStyle,
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0))),
    );
    return textFormField;
  }
  TextFormField getCustomEditTextProfile(
      {
        TextEditingController controller,
        TextInputType keyboardType = TextInputType.text,
        TextStyle textStyle,
        FocusNode focusNode,
        void onChange(String text)
      }) {
    TextFormField textFormField = TextFormField(
      focusNode: focusNode,
      keyboardType: keyboardType,
      style: textStyle,
      controller: controller,
      onChanged: onChange,
      cursorColor: MyColor.tertiary,
      autocorrect: false,
      decoration: InputDecoration(
        focusedBorder:OutlineInputBorder(
          borderSide: const BorderSide(color: MyColor.tertiary, width: 1.0),
          borderRadius: BorderRadius.circular(10.0),

        ),
        contentPadding: EdgeInsets.fromLTRB(16, 0, 16, 0),
        fillColor: Colors.white.withOpacity(0.05),
        filled: true,
        hintStyle: textStyle,
        labelStyle: textStyle,
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0))),
    );
    return textFormField;
  }
  Widget getSearch(
      {
        TextEditingController controller,
        TextInputType keyboardType = TextInputType.text,
        TextStyle textStyle,
        // FocusNode focusNode,
        void onChange(String text)
      }) {
    TextFormField textFormField = TextFormField(
      // focusNode: focusNode,
      keyboardType: keyboardType,
      style: textStyle,
      controller: controller,
      onChanged: onChange,
      cursorColor: MyColor.tertiary,
      autocorrect: false,
      decoration: InputDecoration(
        focusedBorder:OutlineInputBorder(
          borderSide: const BorderSide(color: MyColor.tertiary, width: 1.0),
          borderRadius: BorderRadius.circular(10.0),

        ),
        contentPadding: EdgeInsets.fromLTRB(16, 0, 16, 0),
        fillColor: Colors.white.withOpacity(0.05),
        filled: true,
        hintStyle: textStyle,
        labelStyle: textStyle,
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0))),
    );
    return Stack(
      children: [
        textFormField,
        Positioned(
          right: 15,
          top: 0,
          bottom: 0,
          child: Container(
              width: 20,
              alignment: Alignment.center,
              child: Image.asset("assets/icons/ic_search.png")
          ),
        )
      ],
    );
  }
  TextFormField getCustomEditTextArea(
      {
        TextEditingController controller,
        TextStyle textStyle,
        String hintText,
        FocusNode focusNode,
        void onChange(String text)
      }) {
    TextFormField textFormField = TextFormField(
      focusNode: focusNode,
      minLines: 6,
      maxLines: null,
      keyboardType: TextInputType.multiline,
      style: textStyle,
      autocorrect: false,
      controller: controller,
      onChanged: onChange,
      cursorColor: MyColor.tertiary,
      decoration: InputDecoration(
          focusedBorder:OutlineInputBorder(
          borderSide: const BorderSide(color: MyColor.tertiary, width: 1.0),
          borderRadius: BorderRadius.circular(10.0),
        ),
        contentPadding: EdgeInsets.fromLTRB(16, 8, 16, 8),
        fillColor: Colors.white.withOpacity(0.05),
        filled: true,
        hintText: hintText,
        hintStyle: textStyle,
        labelStyle: textStyle,
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0))),
    );
    return textFormField;
  }
  TextFormField getCustomEditTextAreaFull(
      {
        TextEditingController controller,
        TextStyle textStyle,
        String hintText,
        FocusNode focusNode,
        void onChange(String text)
      }) {
    TextFormField textFormField = TextFormField(
      focusNode: focusNode,
      scrollPadding: EdgeInsets.all(20.0),
      maxLines: null,
      keyboardType: TextInputType.multiline,
      style: textStyle,
      autocorrect: false,
      controller: controller,
      onChanged: onChange,
      cursorColor: MyColor.tertiary,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(16, 8, 16, 8),
        filled: true,
        hintText: hintText,
        hintStyle: textStyle,
        labelStyle: textStyle,
        border: InputBorder.none,
        focusedBorder: InputBorder.none,
        enabledBorder: InputBorder.none,
        errorBorder: InputBorder.none,
        disabledBorder: InputBorder.none
      ),
    );
    return textFormField;
  }
}