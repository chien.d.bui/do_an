import 'dart:typed_data';

import 'package:web3dart/web3dart.dart';
class TransferEvent{
  EthereumAddress to;
  EthereumAddress from;
  var value;
  var nonce;
  Uint8List signature;
  String token;
  TransferEvent({this.to,this.value, this.token, this.signature, this.nonce, this.from});
}