
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:get_storage/get_storage.dart';

import '../model/info_login.dart';


class StorageUtils{
  static const String keyAccessToken = "key_access_token";
  static const String keyUriMTM = "key_uri_metamask";
  static const String keyRefreshToken = "key_refresh_token";
  static const String keyAccount = "key_account";
  static const String keyLogin = "key_login";
  static const String keyPlatform = "key_platform";
  static const String keyListCelebRecent = "key_celeb_recent";
  static const String keyUser = "key_user";
  static const String keyBooker = "key_booker";
  static const String keyLanguage = 'language';
  static const String keyRememberLogin = "key_remember_login";
  static const String keyInfoLogin = "key_info_login";




  static GetStorage _storage;

  static GetStorage get storage {
    _storage ??= GetStorage();
    return _storage;
  }

  static void checkNull() async{
    _storage ??= GetStorage();
  }

  static void saveStorage(String key, dynamic value){
    if(_storage != null){
      _storage.write(key, value);
    }else{
      print(key +"null");
    }
  }

  static String getStorage(String key){
    if(_storage != null){
      return _storage.read(key) ?? "";
    }else{
      print(key +"null");
    }
    return "";
  }

  static void delete(String key){
    if(_storage != null){
      _storage.remove(key);
    }
  }

  static void setAccessToken(String token){
    saveStorage(keyAccessToken, token);
  }

  static String getAccessToken(){
    return getStorage(keyAccessToken);
  }
  static void setUriMTM(String uri){
    saveStorage(keyUriMTM, uri);
  }

  static String getUriMTM(){
    return getStorage(keyUriMTM);
  }
  static void setLanguage(String language){
    saveStorage(keyLanguage, language);
  }

  static String getLanguage(){
    return getStorage(keyLanguage);
  }

  static void setRefreshToken(String refreshToken){
    saveStorage(keyRefreshToken, refreshToken);
  }

  static String getRefreshToken(){
    return getStorage(keyRefreshToken);
  }

  static void setPlatform(String platform){
    saveStorage(keyPlatform, platform);
  }

  static String getPlatform(){
    return getStorage(keyPlatform);
  }

  static void isLogin(bool login){
    _storage.write(keyLogin, login);
  }
  static void isRememberLogin(bool isRemember){
    _storage.write(keyRememberLogin, isRemember);
  }

  static bool getLoginStatus(){
    return _storage.read(keyLogin)?? false;
  }
  static bool getRememberStatus(){
    return _storage.read(keyRememberLogin)?? false;
  }
  static InfoLogin getInfoLogin(){
    InfoLogin infoLogin;
    if(_storage != null){
      var data = _storage.read(keyInfoLogin);
      if(data!=null) {
        infoLogin = InfoLogin.fromJson(jsonDecode(data));
      }
      return infoLogin;
    }else{
      if (kDebugMode) {
        print(keyUser +"null");
      }
      return null;
    }
  }
  static void saveInfoLogin(InfoLogin infoLogin){
    if(_storage != null){
      _storage.write(keyInfoLogin, jsonEncode(infoLogin));
    }else{
      print(keyUser +"null");
    }
  }


}