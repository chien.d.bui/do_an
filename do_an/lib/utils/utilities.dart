
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

/// Created by Bui Duc Chien on
/// 05/08/2021

class Utilities {

  TextEditingController inputMoney(){
    TextEditingController moneyController = TextEditingController();
    moneyController.addListener(() {
      moneyController.value = moneyController.value.copyWith(
        text: moneyController.text.replaceAll('.', '').replaceAllMapped(RegExp(r'(\d{1,3})(?=(\d{3})+(?!\d))'), (Match match) => '${match[1]}.'),
        selection: TextSelection(
          baseOffset: moneyController.text.length,
          extentOffset: moneyController.text.length,
        ),
      );
    });
    return moneyController;
  }
  void copyText(String text){
    Clipboard.setData(new ClipboardData(text: text));
  }
  String convertTime(String input){
    var date = DateTime.fromMillisecondsSinceEpoch(int.parse(input) *1000);
    var dateLocal = date.toLocal();
    return DateFormat('kk:mm | dd/MM/yyyy').format(dateLocal);
  }
}