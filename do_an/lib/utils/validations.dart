

import 'package:get/get_utils/src/get_utils/get_utils.dart';

class Validations{
  static bool checkEmail(String email){
    return GetUtils.isEmail(email);
  }
  static bool checkAddress(String address){
    if(address.length == 42 && address.startsWith("0x")){
      return true;
    }else{
      return false;
    }
  }static bool checkPrivateKey(String privateKey){
    if(privateKey.length == 64 ){
      return true;
    }else{
      return false;
    }
  }
  static bool checkPhone(String phone){
    return GetUtils.isPhoneNumber(phone);
  }
  static bool checkPass(String pass){
    return pass.length>5;
  }
  static bool checkName(String pass){
    if(RegExp(r'[~!@#<>?":_`~;[\]\\|=+)(*&^%0-9-]').hasMatch(pass) || pass.length<3 || pass.length>30){
      return false;
    }else{
      return true;

    }
  }
  static bool checkSocial(String link, type){
    bool check = false;
    switch(type){
      case "facebook":{
        check = link.contains("facebook.com/") || link.contains("fb.com/");
        break;
      }case "instagram":{
        check = link.contains("instagram.com/");
        break;
      }case "twitter":{
        check = link.contains("twitter.com/");
        break;
      }case "tiktok":{
        check = link.contains("tiktok.com/");
        break;
      }case "youtube":{
        check = link.contains("youtube.com/");
        break;
      }
    }
    return check;
  }
}