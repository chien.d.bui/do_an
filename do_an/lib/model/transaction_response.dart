
class TransactionResponse {
  String status;
  String message;
  List<Result> result;
  TransactionResponse({
    this.status,
    this.message,
    this.result,
  });
  TransactionResponse.fromJson(Map<String, dynamic> json) {
    status = json["status"] ?? null;
    message = json["message"] ?? null;
    if(json["result"] != null){
      result = [];
      json["result"].forEach((e){
        result.add(new Result.fromJson(e));
      });
    }
  }
}
class Result{
  String blockNumber;
  String timeStamp;
  String hash;
  String nonce;
  String blockHash;
  String from;
  String contractAddress;
  String to;
  String value;
  String gasUsed;
  Result({
    this.blockNumber,
    this.timeStamp,
    this.hash,
    this.nonce,
    this.blockHash,
    this.from,
    this.to,
    this.value,
    this.gasUsed,
    this.contractAddress
  });
  Result.fromJson(Map<String, dynamic> json){
    contractAddress = json["contractAddress"];
    blockNumber = json["blockNumber"];
    timeStamp = json["timeStamp"];
    hash = json["hash"];
    nonce = json["nonce"];
    blockHash = json["blockHash"];
    from = json["from"];
    to = json["to"];
    value = json["value"];
    gasUsed = json["gasUsed"];
  }
}
