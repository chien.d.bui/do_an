
class InfoLogin {
  String address;
  String privateKey;
  InfoLogin({
    this.address,
    this.privateKey

  });
  InfoLogin.fromJson(Map<String, dynamic> json){
    address = json["address"];
    privateKey = json["privateKey"];
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["address"] = address;
    data["privateKey"] = privateKey;
    return data;
  }

}