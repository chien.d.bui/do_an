// ignore: slash_for_doc_comments
/**
 * Created by Bui Duc Chien on
 * 05/08/2021
 */

class RouterName {
  static const String home = '/home';
  static const String login = '/login';
  static const String transfer = '/transfer';
}