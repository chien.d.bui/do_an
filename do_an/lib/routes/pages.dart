
import 'package:flutter_do_an/page/login/login_binding.dart';
import 'package:flutter_do_an/page/login/login_page.dart';
import 'package:flutter_do_an/page/transfer_coin/transfer_binding.dart';
import 'package:flutter_do_an/page/transfer_coin/transfer_page.dart';
import 'package:flutter_do_an/routes/router_name.dart';
import 'package:get/get.dart';

import '../page/home/home_binding.dart';
import '../page/home/home_page.dart';


/**
 * Created by Bui Duc Chien on
 * 05/08/2021
 */

class Pages {
  static List<GetPage> pages() {
    return [
      GetPage(
        name: RouterName.login,
        page:()=> const LoginPage(),
        binding: LoginBinding()
      ),
      GetPage(
        name: RouterName.home,
        page:()=> HomePage(),
        binding: HomeBinding()
      ),
      GetPage(
        name: RouterName.transfer,
        page:()=> const TransferPage(),
        binding: TransferBinding()
      ),
    ];
  }
}