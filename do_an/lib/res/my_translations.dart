import 'package:flutter/material.dart';
import 'package:flutter_do_an/utils/storage_utils.dart';
import 'package:get/get.dart';

import 'langs/en_us.dart';
import 'langs/vi_vn.dart';


/// created by Bui Duc Chien
/// 05/08/21

class MyTranslations extends Translations {

  // Default locale
  static final locale = Locale('vi');

  // fallbackLocale saves the day when the locale gets in trouble
  static final fallbackLocale = Locale('en');

  static void init() {
    String locale = StorageUtils.getLanguage();
    if(!locale.contains("vi") || !locale.contains("en")) {
      Get.updateLocale(Locale('vi'));
      StorageUtils.setLanguage('vi');
    } else {
      Get.updateLocale(Locale(locale));
    }
  }

  static void updateLocale({String langCode}) {
    Get.updateLocale(Locale(langCode));
    StorageUtils.setLanguage(langCode);
  }


  @override
  Map<String, Map<String, String>> get keys => {
    'vi': viVN,
    'en': enUS
  };
}