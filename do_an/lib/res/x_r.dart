/// created by Bui Duc Chien
/// 05/08/2021

import 'package:get/get.dart';

class XR {
  _String string = _String();
  _AssetsImage assetsImage = _AssetsImage();
}

class _AssetsImage {

  static const String images = 'assets/images/';
}

class _String {
  String title_app = 'title_app'.tr;
  String reload = 'reload'.tr;
  String error_message = 'error_message'.tr;
  String change_lang = 'change_lang'.tr;
  String change_theme = 'change_theme'.tr;

}