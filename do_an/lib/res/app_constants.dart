
import 'package:flutter/material.dart';

import 'my_config.dart';

class Constant{
  static final String OrderCompleted = "completed";
  static final String OrderVerified = "verified";
  static final String OrderEditing = "editing";
  static final String OrderFail = "fail";
  static final String OrderCancel = "cancelled";
  static final String OrderPending = "payment_pending";
  static final String OrderFinish = "payment_finished";
  static final String OrderAccepted = "accepted";
  static final String OrderReject = "rejected";
  //web_view
  static final String TERM = "https://myceleb.vn/dieu-khoan-su-dung";
  static final String FAQ = "https://myceleb.vn/cau-hoi-thuong-gap";
  static final String TERM_PRIVATE = "https://myceleb.vn/chinh-sach-bao-mat";
  static final String INFO = "https://myceleb.vn/gioi-thieu";

  static String getPaymentName(String code){
    switch(code){
      case "completed":
        return "Thành công";
        break;
      case "verified":
        return "Yêu cầu mới";
        break;
      case "editing":
        return "Chờ phê duyệt";
        break;
      case "fail":
        return "Thất bại";
        break;
      case "cancelled":
        return "Huỷ";
        break;
      case "payment_pending":
        return "Chờ thanh toán";
        break;
      case "payment_finished":
        return "Hoàn thành";
        break;
      case "accepted":
        return "Đang tiến hành";
        break;
      case "rejected":
        return "Từ chối";
        break;
      case "received":
        return "Đã nhận hàng";
        break;
      case "pending":
        return "Đang chờ duyệt";
        break;
      case "success":
        return "Đã duyệt";
        break;
      case "deleted":
        return "Từ chối";
        break;
      default:
        return "Đang chờ";
        break;
    }
  }
  static String getPaymentMethod(String code){
    switch(code){
      case "international_card":
        return "Thẻ quốc tế Visa, MasterCard";
        break;
      case "domestic_card":
        return "Thẻ nội địa-Internet Banking";
        break;
      case "bank_transfer":
        return "Chuyển khoản qua ngân hàng";
        break;
      case "gpay_wallet":
        return "Ví điện tử GPay";
        break;
      default:
        return "Đang chờ";
        break;
    }
  }
  static Color getPaymentColor(String code){
    switch(code){
      case "completed":
        return MyColor.success;
        break;
      case "fail":
        return MyColor.red2D;
        break;
      case "deleted":
        return MyColor.red2D;
        break;
      case "cancelled":
        return MyColor.red2D;
        break;
      case "payment_pending":
        return MyColor.pending;
        break;
      case "payment_finished":
        return MyColor.success;
        break;
      case "received":
        return MyColor.success;
        break;
      case "accepted":
        return MyColor.pending;
        break;
      case "rejected":
        return MyColor.red2D;
        break;
      case "success":
        return MyColor.success;
        break;
      default:
        return MyColor.pending;
        break;
    }
  }
  static Text getPaymentText(String code){
    switch(code){
      case "completed":
        return Text("Thành công", style: TextStyle(fontSize: 14, color: MyColor.success),);
        break;
      case "fail":
        return Text("Thất bại", style: TextStyle(fontSize: 14, color: MyColor.red2D),);
        break;
      case "cancelled":
        return Text("Huỷ", style: TextStyle(fontSize: 14, color: MyColor.red2D),);
        break;
      case "payment_pending":
        return Text("Chờ thanh toán", style: TextStyle(fontSize: 14, color: MyColor.pending),);
        break;
      case "payment_finished":
        return Text("Đã thanh toán", style: TextStyle(fontSize: 14, color: MyColor.success),);
        break;
      case "accepted":
        return Text("Đang tiến hành", style: TextStyle(fontSize: 14, color: MyColor.pending),);
        break;
      case "rejected":
        return Text("Từ chối", style: TextStyle(fontSize: 14, color: MyColor.red2D),);
        break;
      case "received":
        return Text("Đã nhận hàng", style: TextStyle(fontSize: 14, color: MyColor.success),);
        break;

      default:
        return Text("Chờ xác nhận", style: TextStyle(fontSize: 14, color: MyColor.pending),);
        break;
    }
  }


}