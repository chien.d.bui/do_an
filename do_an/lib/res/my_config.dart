import 'package:flutter/material.dart';

/// created by Bui Duc Chien
/// 05/08/2021

/// CONFIGS DATA
class MyConfig {
  /// APP CONFIG
  static final String APP_NAME = "-- FLUTTER BASE APP --";
  static final String BASE_URL = "https://yourdomain.com/";
  static final String TOKEN_STRING_KEY = 'BEAR_TOKEN';
  static final String EMAIL_KEY = 'EMAIL_KEY';
  static final String FCM_TOKEN_KEY = 'EMAIL_KEY';

}


/// SPACINGS DATA
class MySpace {
  /// Padding
  static final double paddingZero = 0.0;
  static final double paddingXS = 2.0;
  static final double paddingS = 4.0;
  static final double paddingM = 8.0;
  static final double paddingL = 16.0;
  static final double paddingXL = 24.0;
  static final double paddingXXL = 32.0;

  /// Margin
  static final double marginZero = 0.0;
  static final double marginXS = 2.0;
  static final double marginS = 4.0;
  static final double marginM = 8.0;
  static final double marginL = 16.0;
  static final double marginXL = 24.0;
  static final double marginXXL = 32.0;

  /// Spacing
  static final double spaceXS = 2.0;
  static final double spaceS = 4.0;
  static final double spaceM = 8.0;
  static final double spaceL = 16.0;
  static final double spaceXL = 24.0;
  static final double heightSearchBar = 48.0;

  static double fullWidth(BuildContext context){
    return MediaQuery.of(context).size.width;
  }
  static double fullHeight(BuildContext context){
    return MediaQuery.of(context).size.height;
  }
  static double fullHeightWidget(BuildContext context){
    return MediaQuery.of(context).size.height- kToolbarHeight - kBottomNavigationBarHeight + 6  - MediaQuery.of(context).padding.top;

  }
}

/// COLORS DATA
class MyColor {
  /// Common Colors
  static final PRIMARY_COLOR =  Color(0xFF250048);
  static final MISTY_COLOR =  Color(0xFFE0E0E0);
  static final LIGHT_BACKGROUND_COLOR = Color(0xFFF9F9F9);
  static final ACCENT_COLOR =  Color(0xFF9B51E0);
  static final PRIMARY_VARIANT =  Color(0xFF9B51E0);
  static final PRIMARY_VARIANT_LIGHT =  Color(0xFFE8F5E9);
  static final PRIMARY_SWATCH =  Color(0xFF3681EC);

  static final ICON_COLOR = Colors.white;
  static final TEXT_COLOR =  Color(0xFF000000);
  static final BUTTON_COLOR =  PRIMARY_COLOR;
  static final TEXT_BUTTON_COLOR =  Colors.white;


  static final PRIMARY_DARK_COLOR =  Color(0xFF250048);
  static final Color DARK_BACKGROUND_COLOR = Color(0xFF000000);
  static final ICON_COLOR_DARK = Colors.white;
  static final TEXT_COLOR_DARK =  Color(0xFFffffff);
  static final BUTTON_COLOR_DARK =  PRIMARY_DARK_COLOR;
  static final TEXT_BUTTON_COLOR_DARK =  Colors.black;

  static final Color black26 = Color(0xff262626);
  static final Color black59 = Color(0xff595959);
  static final Color grayD9 = Color(0xffD9D9D9);
  static final Color gray8C = Color(0xff8C8C8C);
  static final Color grayF5 = Color(0xfff5f5f5);
  static final Color grayF1 = Color(0xfff1f1f1);
  static final Color grayE8 = Color(0xffe8e8e8);
  static final Color grayDD = Color(0xffDDDDDD);
  static final Color grayBF = Color(0xffBFBFBF);
  static final Color grayFA = Color(0xfffafafa);
  static final Color grayCC = Color(0xffcccccc);
  static final Color gray89 = Color(0xff898989);

  static const Color orange = Color(0xfffa652f);
  static const Color orange16 = Color(0xffFA8C16);
  static const Color orange17 = Color(0xffFF8917);
  static const Color orangeE6 = Color(0xffFFF7E6);
  static const Color orange69 = Color(0xffFFC069);
  static const Color orangeFF = Color(0xffFFB500);
  static const Color orange516 = Color(0xfffab516);
  static const Color orange00 = Color(0xff873800);
  static const Color orange05 = Color(0xffFF5B05);

  static const Color blue18 = Color(0xff1890FF);
  static const Color blue8C = Color(0xff003A8C);
  static const Color blue09 = Color(0xff096DD9);
  static const Color blue91 = Color(0xff91D5FF);
  static const Color blueE6 = Color(0xffE6F7FF);
  static const Color blue40 = Color(0xff40A9FF);
  static const Color blue08   = Color(0xff08979C);
  static const Color blueFB = Color(0xffE6FFFB);
  static const Color blueB3 = Color(0xff0050B3);

  static const Color background = Color(0xff191623);
  static const Color black = Colors.black;
  static const Color iconColor = Color(0xffa8a09b);
  static const Color lightGrey = Color(0xffE1E2E4);
  static const Color titleTextColor = const Color(0xff1d2635);

  static const Color green38 = Color(0xff389E0D);
  static const Color green73 = Color(0xff73D13D);
  static const Color greenF6 = Color(0xffF6FFED);
  static const Color green52 = Color(0xff52C41A);
  static const Color green64 = Color(0xff95DE64);

  static const Color red2D = Color(0xffF5222D);
  static const Color redF0 = Color(0xffFFF1F0);
  static const Color red9E = Color(0xffFFA39E);

  static const Color primary = Color(0xffFFC204);
  static const Color secondary = Color(0xffFF057D);
  static const Color tertiary = Color(0xffFF6E1E);
  static const Color content = Color(0xffCBC4D8);
  static const Color text = Color(0xff867F96);
  static const Color success = Color(0xff10BF41);
  static const Color pending = Colors.yellow;
  static const Color border = Color(0xff6F649B);
  static const Color bgSecondary = Color(0xff252242);
  static Color backgroundBtn = Color(0xffFF6E1E).withOpacity(0.1);
  static Color backgroundSearch = Color(0xff3F3B65);
  static Color hinText = Color(0xffCBC4D8);
  static Color borderTransfer = Color(0xff7046ec);
  static Color transparent = Colors.transparent;

  static final gradientPrimary = LinearGradient(colors: [
    Color(0xffFFC204),
    Color(0xffFF057D),
    ],
    begin: Alignment(-2,0),
    end: Alignment(1,0),
  );
  static final gradientGray = LinearGradient(colors: [
    Color(0xff8C8C8C),
    Color(0xff8C8C8C),
    ],
    begin: Alignment(-2,0),
    end: Alignment(1,0),
  );
  static final gradientSecondary = LinearGradient(colors: [
    Color(0xffFC4AF5),
    Color(0xff0846E4),
  ]);
  static const colorizeColors = [
    MyColor.tertiary,
    Colors.purple,
    Colors.blue,
    Colors.yellow,
    Colors.red,

  ];
}