import 'package:flutter/material.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import '../../base/base_view_view_model.dart';
import '../../res/my_config.dart';
import '../../widgets/edit_text_utils.dart';
import '../../widgets/expanded_widget.dart';
import 'login_controller.dart';
class LoginPage extends BaseViewModel<LoginController> {
  const LoginPage({Key key}) : super(key: key);
  @override
  Widget vmBuilder(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height- kToolbarHeight - MediaQuery.of(context).padding.top;
    return GestureDetector(
      onTap: ()=> FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
        backgroundColor: MyColor.background,
        appBar: AppBar(
          backgroundColor: MyColor.background,
          title: Text("login_title".tr,
            style: const TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 17
            ),
          ),
          centerTitle: true,
        ),
        body: SafeArea(
          child: SizedBox(
            height: height,
            child: Stack(
              clipBehavior: Clip.none,
              children: [
                SingleChildScrollView(
                  keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
                  child: Container(
                    padding: const EdgeInsets.fromLTRB(15, 28, 15, 0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: 52,
                          child: EditTextUtils().getCustomEditTextLogin(
                            labelValue: "Address".tr,
                            controller: controller.emailController,
                            obscureText: false,
                            focusNode: controller.fcEmail
                          ),
                        ),
                        controller.checkMail.value?
                        Container()
                        :ExpandedSection(
                          axis: Axis.horizontal,
                          expand: !controller.checkMail.value,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8.0),
                            child: Text(
                              "Address không hợp lệ!",
                              style: textStyleNormalCustom(12, color: MyColor.red2D),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                          child: Container(
                              height: 52,
                              alignment: Alignment.center,
                              child: EditTextUtils().getCustomEditTextLogin(
                                  labelValue: "PrivateKey".tr,
                                  controller: controller.passController,
                                  obscureText: controller.obscureText.value,
                                  icon: GestureDetector(
                                    onTap: (){
                                      controller.togglePass();
                                    },
                                    child: Container(width: 25, alignment: Alignment.center,child: controller.obscureText.value ? Image.asset("assets/image/ic_hide.png", width: 25, color: Colors.white, fit: BoxFit.cover,) : Image.asset("assets/image/ic_show.png", width: 25, color: Colors.white)),
                                  ),
                                focusNode: controller.fcPass
                              )
                          ),
                        ),
                        controller.checkPass.value?
                        Container()
                        :ExpandedSection(
                          axis: Axis.horizontal,
                          expand: !controller.checkPass.value,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8.0),
                            child: Text(
                              "PrivateKey không hợp lệ!",
                              style: textStyleNormalCustom(12, color: MyColor.red2D),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  SizedBox(
                                    height: 20,
                                    width: 20,
                                    child: Checkbox(
                                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                      checkColor: Colors.white,
                                      activeColor: MyColor.tertiary,
                                      value: controller.isCheck.value,
                                      side: const BorderSide(
                                          color: MyColor.tertiary
                                      ),
                                      onChanged: (bool value) {
                                        controller.isCheck.value = value;
                                      },
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 8.0),
                                    child: Text("Ghi nhớ tôi", style: textStyleNormalCustom(14),),
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                        Padding(
                            padding: const EdgeInsets.fromLTRB(0, 32, 0, 0),
                            child: SizedBox(
                              width: width,
                              height: 40,
                              child: RoundedLoadingButton(
                                borderRadius: 12,
                                color: Colors.red,
                                controller: controller.btnController,
                                onPressed: () {
                                  FocusManager.instance.primaryFocus?.unfocus();
                                  controller.onClickLogin(context);
                                },
                                child: Container(
                                  width: width,
                                  height: 40,
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                    gradient: MyColor.gradientPrimary,
                                    borderRadius: BorderRadius.circular(12)
                                  ),
                                  child: Text(
                                    "login".tr,
                                    style: textStyleTitle(14)
                                  ),
                                ),
                              ),
                            )
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
