
import 'dart:async';

import 'package:eth_sig_util/eth_sig_util.dart';
import 'package:event_bus/event_bus.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_do_an/model/info_login.dart';
import 'package:flutter_do_an/res/my_res.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:web3dart/web3dart.dart';
import 'dart:io' as io;
import '../../base/base_controller.dart';
import '../../utils/storage_utils.dart';
import '../../utils/validations.dart';
import 'login_repository.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:flutter_do_an/base/base_view_view_model.dart';
import 'package:flutter_do_an/utils/storage_utils.dart';
import 'package:http/http.dart';

class LoginController extends BaseController<LoginRepository>{
  var obscureText = true.obs;
  bool isIos = io.Platform.isIOS;
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passController = TextEditingController();
  TextEditingController forgotPassController = TextEditingController();
  String verifyValue;
  String feature;
  String type;
  String newPass;
  final _eventBus = Get.find<EventBus>();
  var isCheck = false.obs;
  final btnController = RoundedLoadingButtonController();
  var checkMail = true.obs;
  var checkPass = true.obs;
  Web3Client ethClient;
  FocusNode fcEmail = FocusNode();
  FocusNode fcPass = FocusNode();
  String addressLogin = "0x15f171bA291eE9795C5e55dc9837150e779647de";
  Credentials credentials;
  ContractEvent loginEvent;
  String account;
  String privateKey;
  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    emailController.text = "0x7d7eea7559E2e27de9d823f50E3A4cb771F66116";
    passController.text = "ff8140a27ec900ccfe51fd2e00554a27983f2446bcec4460a8d29c7d3abef7bf";
    ethClient = Web3Client("https://kovan.infura.io/v3/f4e05aef1661452d915d34547435604d", Client());
    if (StorageUtils.getRememberStatus() != null) {
      isCheck.value = StorageUtils.getRememberStatus();
    }
    if (isCheck.value) {
      if (StorageUtils.getInfoLogin() != null) {
        InfoLogin infoLogin = StorageUtils.getInfoLogin();
        emailController.text = infoLogin.address;
        passController.text = infoLogin.privateKey;
      }
    }
  }
  void togglePass(){
    obscureText.value = !obscureText.value;
  }
  Future onClickLogin(BuildContext context) async {
    String userName = emailController.text.trim();
    String pass = passController.text;
    if(!Validations.checkAddress(userName)){
      checkMail.value = false;
      btnController.stop();
      FocusScope.of(context).requestFocus(fcEmail);
      return;
    }
    checkMail.value = true;
    if(!Validations.checkPrivateKey(pass)){
      checkPass.value = false;
      btnController.stop();
      FocusScope.of(context).requestFocus(fcPass);
      return;
    }
    hash();
  }
  Future<String> hash() async{
    try{
      account = emailController.text;
      privateKey = passController.text;
      print(addressLogin.length);
      print(privateKey.length);
      credentials = EthPrivateKey.fromHex(privateKey);
      List<String> types = ["address", "uint256"];
      BigInt nonce = BigInt.from(await ethClient.getTransactionCount(EthereumAddress.fromHex(account), atBlock: BlockNum.pending()));
      final messageHash = AbiUtil.soliditySHA3(types,
          [EthereumAddress.fromHex(account).addressBytes, nonce]);
      final signature = await credentials.signPersonalMessage(messageHash);
      //
      var response = await sendHash("login", [EthereumAddress.fromHex(account), nonce, signature]);
      return response;
    }catch(e){
      showErrorSnackBar(message: "Vui lòng kiểm tra lại tài khoản");
      btnController.reset();
    }
  }
  Future<DeployedContract> loadBridgeHash() async{
    String abiStringFile = await rootBundle.loadString("assets/abi/abiLogin.json");
    final contract = DeployedContract(ContractAbi.fromJson(abiStringFile, "Login"), EthereumAddress.fromHex(addressLogin));
    loginEvent = contract.event('Transfer');
    return contract;
  }
  Future<String>  sendHash(String functionName, List<dynamic> args) async{
    DeployedContract contract = await loadBridgeHash();
    final ethFunction = contract.function(functionName);
    String result;
    // await launch(uriOpen).whenComplete(() async{
    print("Send.....");
    result = await ethClient.sendTransaction(
        credentials,
        Transaction.callContract(
          from: EthereumAddress.fromHex(account),
          contract: contract,
          function: ethFunction, parameters: args,
          gasPrice: EtherAmount.fromUnitAndValue(EtherUnit.kwei, 2000000),
          maxGas: 100000,
          value: EtherAmount.fromUnitAndValue(EtherUnit.finney, 0),
          nonce: await ethClient.getTransactionCount(EthereumAddress.fromHex(account), atBlock: BlockNum.pending()),
        ), fetchChainIdFromNetworkId: false, chainId: 42
    ).catchError((e){
      print(e);
      showErrorSnackBar(message: "Tài khoản bạn vừa nhập không chính xác hoặc số dư không đủ");
      btnController.reset();
      // });
    });
    ethClient
        .events(FilterOptions.events(contract: contract, event: loginEvent))
        .take(1)
        .listen((event) async {
      final decoded = loginEvent.decodeResults(event.topics, event.data);
      final result = decoded[0] as bool;
      final message = decoded[1] ;
      final signature = decoded[2] ;
      if (kDebugMode) {
        print("Event nè... $result... $message... $signature");
        showSuccessSnackBar(message: "Đăng nhập thành công");
        StorageUtils.isLogin(true);
        StorageUtils.saveInfoLogin(InfoLogin(address: emailController.text, privateKey: passController.text));
        StorageUtils.isRememberLogin(isCheck.value);
        btnController.reset();
        Get.offNamed(RouterName.home);
      }
    });
    return result;
  }
}