import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';

import '../../base/base_view_view_model.dart';
import '../../res/my_config.dart';
import '../../utils/measure_size.dart';
import '../../widgets/edit_text_utils.dart';
import 'transfer_controller.dart';

class TransferPage extends BaseViewModel<TransferController>  {
  const TransferPage({Key key}) : super(key: key);

  @override
  Widget vmBuilder(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColor.background,
      appBar: AppBar(
        backgroundColor: MyColor.background,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios_outlined, color: Colors.white),
          onPressed: () => Get.back(),
        ),
        title: Text("Chuyển tiền".tr,
          style: const TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 17
          ),
        ),
        centerTitle: true,
      ),
      body: SafeArea(
          child:
          SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Row(
                      children: [
                        Container(
                          width: 15,
                          height: 15,
                          margin: EdgeInsets.only(right: 10),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              border: Border.all(width: 3, color: MyColor.tertiary),
                              color: Colors.white
                          ),
                        ),
                        Expanded(child: Text("Bước 1: Nhâp số tiền",style: textStyleTitleCustom(16,color: MyColor.tertiary))),
                        AnimatedOpacity(
                          alwaysIncludeSemantics: true,
                          opacity: controller.step.value == 1? 0: 1,
                          duration: const Duration(milliseconds: 1000),
                          child: controller.step.value == 1? Container():
                          GestureDetector(
                            onTap: ()=>controller.step.value = 1,
                            child: Row(
                              children: [
                                // Image.asset('assets/image/ic_edit.png',width: 17,height: 17,),
                                Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Text('Thay đổi', style: textStyleNormal(15)),
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Stack(
                      children: [
                        Align(alignment: Alignment.centerLeft,
                            child: Container(margin: EdgeInsets.only(left: 7.25),height: controller.step.value == 1?controller.heightOne.value:controller.heightOneDone.value,
                                child: VerticalDivider(width: 1,color: MyColor.tertiary,))),
                        Padding(
                          padding: const EdgeInsets.only(left: 23.0),
                          child: AnimatedCrossFade(
                            duration: const Duration(milliseconds: 500),
                            firstChild: MeasureSize(
                                onChange: (size){
                                  controller.heightOneDone.value = size.height;
                                },
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 8.0, bottom: 8),
                                  child: Text(controller.moneyController.text+controller.from, style: textStyleTitleCustom(15),),
                                )
                            ),
                            secondChild: MeasureSize(
                                onChange: (size){
                                  controller.heightOne.value = size.height;
                                },
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 8.0, bottom: 16),
                                  child: EditTextUtils().getCustomEditTextNormal(controller: controller.moneyController, hintText: "vd: 10 "+controller.from, textStyle: textStyleNormalCustom(15),
                                    keyboardType: TextInputType.number,
                                  ),
                                )
                            ),
                            crossFadeState: controller.step.value >1? CrossFadeState.showFirst : CrossFadeState.showSecond,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Row(
                      children: [
                        Container(
                          width: 15,
                          height: 15,
                          margin: EdgeInsets.only(right: 10),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              border: Border.all(width: 3, color: controller.step.value < 2? MyColor.border: MyColor.tertiary),
                              color: Colors.white
                          ),
                        ),
                        Expanded(child: Text("Bước 2: Chọn mạng",style: textStyleTitleCustom(16,color:controller.step.value < 2? MyColor.text: MyColor.tertiary))),
                        AnimatedOpacity(
                          alwaysIncludeSemantics: true,
                          opacity: controller.step.value == 1? 0: 1,
                          duration: const Duration(milliseconds: 1000),
                          child: controller.step.value == 2? Container():
                          GestureDetector(
                            onTap: ()=>controller.step.value = 2,
                            child: Row(
                              children: [
                                // Image.asset('assets/image/ic_edit.png',width: 17,height: 17,),
                                Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Text('Thay đổi', style: textStyleNormal(15)),
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Stack(
                      children: [
                        Align(alignment: Alignment.centerLeft,
                            child: Container(margin: EdgeInsets.only(left: 7.25),height:controller.step.value == 1? 0: controller.step.value ==2? controller.heightTwo.value:controller.heightTwoDone.value,
                                child: VerticalDivider(width: 1,color: controller.step.value < 2? MyColor.border: MyColor.tertiary,))),
                        Padding(
                          padding: const EdgeInsets.only(left: 23.0),
                          child: AnimatedCrossFade(
                            duration: const Duration(milliseconds: 500),
                            firstChild: controller.step<2 ?Container():
                            MeasureSize(
                                onChange: (size){
                                  controller.heightTwoDone.value = size.height;
                                },
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 8.0, bottom: 8),
                                  child: Text("Mạng: "+controller.textType.value, style: textStyleTitleCustom(15),),
                                )
                            ),
                            secondChild: MeasureSize(
                                onChange: (size){
                                  controller.heightTwoDone.value = size.height;
                                },
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 8.0, bottom: 8),
                                  child: PopupMenuButton<bool>(
                                    color: MyColor.background,
                                    onSelected: (bool value) {
                                      FocusManager.instance.primaryFocus?.unfocus();
                                      controller.isEth.value = value;
                                      if(value){
                                        controller.textType.value = "Etherium";
                                      }else{
                                        controller.textType.value = "Binance";
                                      }
                                    },
                                    child: Padding(
                                      padding: const EdgeInsets.only(right: 16.0),
                                      child: Container(
                                        padding: EdgeInsets.symmetric(vertical: 8, horizontal: 12),
                                        decoration: BoxDecoration(
                                          color: MyColor.bgSecondary,
                                          borderRadius: BorderRadius.circular(8),
                                        ),
                                        child: Text(controller.textType.value,style: textStyleTitleCustom(15))),
                                    ),
                                    itemBuilder: (context)=> <PopupMenuEntry<bool>>[
                                      const PopupMenuItem<bool>(
                                        value: true,
                                        child: Text("Etherium",style: TextStyle(color: Colors.white)),
                                      ),
                                      const PopupMenuItem<bool>(
                                        value: false,
                                        child: Text("Binance",style: TextStyle(color: Colors.white)),
                                      ),
                                    ],
                                  ),
                                )
                            ),
                            crossFadeState: controller.step.value != 2? CrossFadeState.showFirst : CrossFadeState.showSecond,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Row(
                      children: [
                        Container(
                          width: 15,
                          height: 15,
                          margin: EdgeInsets.only(right: 10),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              border: Border.all(width: 3, color: controller.step.value != 3? MyColor.border: MyColor.tertiary),
                              color: Colors.white
                          ),
                        ),
                        Expanded(child: Text("Bước 3: Địa chỉ nhận tiền",style: textStyleTitleCustom(16,color:controller.step.value != 3? MyColor.text: MyColor.tertiary))),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Stack(
                      children: [
                        Align(alignment: Alignment.centerLeft,
                            child: Container(margin: EdgeInsets.only(left: 7.25),height:controller.step.value != 3? 0: controller.heightThree.value,
                                child: VerticalDivider(width: 1,color: controller.step.value <3? MyColor.border: MyColor.tertiary,))),
                        Padding(
                          padding: const EdgeInsets.only(left: 23.0),
                          child: AnimatedCrossFade(
                            duration: const Duration(milliseconds: 500),
                            firstChild: Container(),
                            secondChild: MeasureSize(
                                onChange: (size){
                                  controller.heightThree.value = size.height;
                                },
                                child: Column(
                                  children: [
                                    Row(
                                      children: [
                                        CupertinoSwitch(
                                          activeColor: MyColor.tertiary,
                                          value: controller.lights.value,
                                          onChanged: (bool value) { controller.lights.value = value; },
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(left: 8.0),
                                          child: Text("Cho tôi", style: textStyleNormalCustom(15),),
                                        ),
                                      ],
                                    ),
                                    controller.lights.value?
                                    Container():
                                    Padding(
                                      padding: const EdgeInsets.only(top: 8.0, bottom: 16),
                                      child: EditTextUtils().getCustomEditTextNormal(controller: controller.addressController, textStyle: textStyleNormalCustom(15), hintText: "Nhập địa chỉ người nhận"),
                                    )
                                  ],
                                )
                            ),
                            crossFadeState: controller.step.value < 3? CrossFadeState.showFirst : CrossFadeState.showSecond,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.only(top: 16),
                      width: MySpace.fullWidth(context) - 30,
                      height: 40,
                      child: RoundedLoadingButton(
                        borderRadius: 12,
                        color: Colors.red,
                        controller: controller.buttonController,
                        onPressed: ()=> {
                          FocusManager.instance.primaryFocus?.unfocus(),
                          controller.logicContinue()
                        },
                        child: Container(
                          width: MySpace.fullWidth(context) - 30,
                          height: 40,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              gradient: MyColor.gradientPrimary,
                              borderRadius: BorderRadius.circular(12)
                          ),
                          child: Text(
                              "next".tr,
                              style: textStyleTitle(14)
                          ),
                        ),
                      )
                  )
                ],
              ),
            ),
          )
      ),
    );
  }

}
