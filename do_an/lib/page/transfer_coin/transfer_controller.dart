
import 'package:flutter/material.dart';
import 'package:flutter_do_an/page/home/bscWallet/bsc_wallet_controller.dart';
import 'package:flutter_do_an/utils/validations.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:web3dart/credentials.dart';

import '../../base/base_controller.dart';
import '../home/ethWallet/eth_wallet_controller.dart';
import 'transfer_repository.dart';

class TransferController extends BaseController<TransferRepository>{
  var indexPayment = 1.obs;
  var payment = "international_card".obs;
  var isLoading = false.obs;
  var step = 1.obs;
  var heightOne = 0.0.obs;
  var heightOneDone = 0.0.obs;
  var heightTwo = 0.0.obs;
  var heightTwoDone = 0.0.obs;
  var heightThree = 0.0.obs;
  var heightThreeDone = 0.0.obs;
  var isEth = false.obs;
  var lights = true.obs;
  var textType = "Mạng".obs;
  final buttonController = RoundedLoadingButtonController();
  TextEditingController moneyController;
  TextEditingController addressController = TextEditingController();
  String from;
  String account;
  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    from = Get.arguments["from"];
    account = Get.arguments["address"];
    addressController.text = "0x7d7eea7559E2e27de9d823f50E3A4cb771F66116";
    moneyController = inputMoney();
  }
  void logicContinue(){
    switch(step.value){
      case 1:{
        if(moneyController.text.isEmpty){
          showErrorSnackBar(message: "Vui lòng nhập số tiền cần chuyển!");
          buttonController.stop();
          return;
        }
        step++;
        buttonController.stop();
        break;
      }
      case 2:{
        if(textType.value == "Mạng"){
          showErrorSnackBar(message: "Vui lòng chọn mạng!");
          buttonController.stop();
          return;
        }
        step++;
        buttonController.stop();
        break;
      }
      default :{
        if(!Validations.checkAddress(addressController.text)){
          showErrorSnackBar(message: "Vui lòng nhập địa chỉ hợp lệ!");
          buttonController.stop();
          return;
        }
        String to;
        if(lights.value){
          to = account;
        }else{
          to = addressController.text;
        }
        if(from == "Eth") {
          final ethController = Get.find<ETHWalletController>();
          ethController.buttonController.start();
          ethController.isTransfer = true;
          if(isEth.value){
            ethController.transfer(
                EthereumAddress.fromHex(to),
                BigInt.from(double.parse(moneyController.text)));
          }else {
            ethController.transferCoinEth(EthereumAddress.fromHex(account),
                EthereumAddress.fromHex(to),
                BigInt.from(double.parse(moneyController.text)), "burn");
          }
        }else{
          final bscController = Get.find<BSCWalletController>();
          bscController.isTransfer = true;
          bscController.buttonController.start();
          if(!isEth.value){
            bscController.transfer(
                EthereumAddress.fromHex(to),
                BigInt.from(double.parse(moneyController.text)));
          }else {
            bscController.transferCoinBsc(EthereumAddress.fromHex(account),
                EthereumAddress.fromHex(to),
                BigInt.from(double.parse(moneyController.text)), "burn");
          }
        }
        buttonController.stop();
        Get.back();
        break;

      }
    }
  }


}