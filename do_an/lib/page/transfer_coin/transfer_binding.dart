import 'package:get/get.dart';
import 'transfer_controller.dart';
import 'transfer_repository.dart';

class TransferBinding extends Bindings{
  @override
  void dependencies() {
    Get.put(TransferRepository());
    Get.put(TransferController());
  }

}