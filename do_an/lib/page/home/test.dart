
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_do_an/base/base_view_view_model.dart';
import 'package:flutter_do_an/res/my_config.dart';
import 'package:flutter_do_an/utils/wallet_connect_eth_credentials.dart';
import 'package:flutter_do_an/widgets/edit_text_utils.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:walletconnect_dart/walletconnect_dart.dart';
import 'package:web3dart/web3dart.dart';
import 'package:http/http.dart';

import 'home_repository.dart';

class HomeController extends BaseController<HomeRepository>{
  Web3Client ethClient;
  var isLoading = true.obs;
  final myAddress = '0x7d7eea7559E2e27de9d823f50E3A4cb771F66116';
  var myData;
  num money  = 0;
  var transaction = ''.obs;
  final refreshController = RoundedLoadingButtonController();
  final depositController = RoundedLoadingButtonController();
  final withdrawController = RoundedLoadingButtonController();
  final randomController = RoundedLoadingButtonController();
  final joinController = RoundedLoadingButtonController();
  final playController = RoundedLoadingButtonController();
  TextEditingController nameController = TextEditingController();
  SessionStatus session;
  String account;
  WalletConnectEthereumCredentials credentials;
  @override
  void onInit() async{
    // TODO: implement onInit
    super.onInit();
    // getBalance();
    walletConnect();
  }
  Future<DeployedContract> loadContract() async{
    String abi = await rootBundle.loadString("assets/abi.json");
    String contractAddress = "0xA45d2C8d22711BAb2531f34e528Da22537C4800e";
    final contract = DeployedContract(ContractAbi.fromJson(abi, "GameXoSo"), EthereumAddress.fromHex(contractAddress));
    return contract;

  }
  walletConnect() async {
    final connector = WalletConnect(
      bridge: 'https://bridge.walletconnect.org',
      clientMeta: const PeerMeta(
        name: 'WalletConnect',
        description: 'WalletConnect Developer App',
        url: 'https://walletconnect.org',
        icons: [
          'https://gblobscdn.gitbook.com/spaces%2F-LJJeCjcLrr53DcT1Ml7%2Favatar.png?alt=media'
        ],
      ),
    );
    // Subscribe to events
    connector.on('connect', (session) => print(session));
    connector.on('session_update', (payload) => print(payload));
    connector.on('disconnect', (session) => print(session));

    // Create a new session
    if (!connector.connected) {
      session = await connector.createSession(
          chainId: 4,
          onDisplayUri: (uri) async => {print(uri), await launch(uri)});
    }

    account = session.accounts[0];

    if (account != null) {
      print("Success: "+account);
      if (account != null) {
        ethClient = Web3Client("https://kovan.infura.io/v3/f4e05aef1661452d915d34547435604d", Client());
        EthereumWalletConnectProvider provider =
        EthereumWalletConnectProvider(connector);
        credentials = WalletConnectEthereumCredentials(provider: provider);
      }
    }
  }
  Future<List<dynamic>> query(String functionName, List<dynamic> args) async{
    final contract = await loadContract();
    final ethFunction = contract.function(functionName);
    final result = await ethClient.call(contract: contract, function: ethFunction, params: args);
    return result;
  }
  Future getBalance() async{
    isLoading.value = true;
    // EthereumAddress address = EthereumAddress.fromHex(targetAddress);
    List<dynamic> result = await query("getBalance", []);
    myData = result[0];
    myData = EtherAmount.fromUnitAndValue(EtherUnit.wei, myData).getValueInUnit(EtherUnit.ether);
    isLoading.value = false;
    refreshController.stop();
  }
  Future randomNumber() async{
    // EthereumAddress address = EthereumAddress.fromHex(targetAddress);
    String result = await submit("getRandomNumber", []);
    showSuccessSnackBar(message: "Số random là: "+ result);
    randomController.stop();
  }
  Future<String> sendCoin() async{
    var bigAmount = BigInt.from(money);
    var response = await submit("depositBalance", [bigAmount]);
    showSuccessSnackBar(message: "Nạp ${bigAmount}\$ thành công!");
    transaction.value = response;
    depositController.stop();
    return response;
  }
  Future<String> withDrawCoin() async{
    var bigAmount = BigInt.from(money);
    var response = await submit("withdrawBalance", [bigAmount]);
    showSuccessSnackBar(message: "Rút ${bigAmount}\$ thành công!");
    transaction.value = response;
    withdrawController.stop();
    return response;
  }
  Future<String> submit(String functionName, List<dynamic> args) async{
    // EthPrivateKey credentials = EthPrivateKey.fromHex("ff8140a27ec900ccfe51fd2e00554a27983f2446bcec4460a8d29c7d3abef7bf");
    DeployedContract contract = await loadContract();
    final ethFunction = contract.function(functionName);
    final result = await ethClient.sendTransaction(credentials, Transaction.callContract(
        from: EthereumAddress.fromHex(account),
        contract: contract,
        function: ethFunction, parameters: args, gasPrice: EtherAmount.inWei(BigInt.one),
        maxGas: 100000,
        value: EtherAmount.fromUnitAndValue(EtherUnit.finney, 1)), fetchChainIdFromNetworkId: true, chainId: 4);
    return result;
  }
  void showActionJoin(){
    var val = 1.obs;
    joinController.stop();
    showDialogNormal(
      title: "Join Game",
      desc: "Please enter name and your choice!",
      content: Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: Obx(()=>Column(
          children: [
            EditTextUtils().getCustomEditTextLogin(
                labelValue: "Your Name",
                controller: nameController,
                obscureText: false,
                size: 13
            ),
            ListTile(
              title: Text("Chẵn", style: TextStyle(color: MyColor.content, fontSize: 15),),
              leading: Radio(
                value: 2,
                groupValue: val.value,
                onChanged: (value) {
                  val.value = value;
                },
                activeColor: MyColor.tertiary,
              ),
            ),
            ListTile(
              title: Text("Lẻ", style: TextStyle(color: MyColor.content, fontSize: 15)),
              leading: Radio(
                value: 1,
                groupValue: val.value,
                onChanged: (value) {
                  val.value = value;
                },
                activeColor: MyColor.tertiary,
              ),
            )
          ],
        )),
      ),
      onClickOke: (){
        joinGame(nameController.text, val.value);
      },
    );
  }
  Future joinGame(String name, int number) async{
    var bigAmount = BigInt.from(number);
    // EthereumAddress address = EthereumAddress.fromHex(targetAddress);
    var response = await submit("join", [name, bigAmount]);
    showSuccessSnackBar(message: "Tham gia thành công");
    transaction.value = response;
    print(response);
    joinController.stop();
  }
  Future play() async{
    // EthereumAddress address = EthereumAddress.fromHex(targetAddress);
    var response = await submit("play", []);
    showSuccessSnackBar(message: "Bắt đầu game");
    transaction.value = response;
    print(response);
    playController.stop();
  }


}