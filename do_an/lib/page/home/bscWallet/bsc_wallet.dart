import 'package:flutter/material.dart';
import 'package:flutter_do_an/base/base_view_view_model.dart';
import 'package:flutter_do_an/res/my_config.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:shimmer/shimmer.dart';
import '../../../model/transaction_response.dart';
import '../../../routes/router_name.dart';
import '../../../widgets/expanded_widget.dart';
import 'bsc_wallet_controller.dart';
class BSCWalletPage extends BaseViewModel<BSCWalletController> {
  const BSCWalletPage({Key key}) : super(key: key);
  @override
  Widget vmBuilder(BuildContext context) {
    return GestureDetector(
      onTap: ()=> FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
        backgroundColor: MyColor.background,
        body: RefreshIndicator(
          onRefresh: () async{
            controller.getBalanceBsc();
            controller.getListTransaction();
          },
          child: Stack(
            children: [
              SingleChildScrollView(
                controller: controller.scrollController,
                physics: AlwaysScrollableScrollPhysics(),
                child: Column(
                  children: [
                    Stack(
                      children: [
                        Container(
                          decoration: BoxDecoration(
                            gradient: MyColor.gradientPrimary
                          ),
                          width: MySpace.fullWidth(context),
                          height: 200,
                          margin: EdgeInsets.only(bottom: 100),
                        ),
                        Positioned(
                          top: 100,
                          left: 0,
                          right: 0,
                          child: Center(
                            child: Container(
                              margin: const EdgeInsets.only(right: 8),
                              padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 12),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: MyColor.red2D
                              ),
                              child: Text("Binance",style: textStyleTitle(20)),
                            ),
                          ),
                        ),
                        Positioned(
                            top: 160,
                            right: 0,
                            left: 0,
                            child: Container(
                              margin: EdgeInsets.symmetric(horizontal: 16),
                              height: 150,
                              decoration: BoxDecoration(
                                  color: MyColor.bgSecondary,
                                  borderRadius: BorderRadius.circular(12),
                                  boxShadow: [
                                    BoxShadow(
                                        color: Colors.black.withOpacity(0.2),
                                        blurRadius: 2,
                                        spreadRadius: 2,
                                        offset: Offset(0, 2)
                                    )
                                  ]
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                                    child: Text("Balance", style: textStyleTitleCustom(15),),
                                  ),
                                  controller.isLoading.value?
                                  const Padding(
                                    padding: EdgeInsets.symmetric(vertical: 16.0),
                                    child: CircularProgressIndicator(color: MyColor.tertiary),
                                  ):
                                  Padding(
                                      padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 16),
                                      child: Shimmer.fromColors(
                                          baseColor: Colors.red,
                                          highlightColor: Colors.yellow,
                                          child: Text("\$${controller.myData} BSC", style: textStyleTitleCustom(35), overflow: TextOverflow.ellipsis, maxLines: 1,))
                                  )
                                ],
                              ),
                            )
                        ),
                        Positioned(
                            top: 40,
                            right: 15,
                            child: InkWell(
                              onTap: (){
                                controller.logout();
                              },
                              child: Container(
                                padding: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(8)
                                ),
                                child: Icon(Icons.logout, color: MyColor.gray8C, size: 25),
                              ),
                            )
                        )
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          SizedBox(
                            width: 120,
                            height: 40,
                            child: RoundedLoadingButton(
                              borderRadius: 12,
                              color: Colors.red,
                              controller: controller.buttonController,
                              onPressed: (){
                                if(!controller.isTransfer) {
                                  controller.buttonController.reset();
                                  Get.toNamed(RouterName.transfer, arguments: {
                                    'from': "Bsc",
                                    'address': controller.account.value
                                  });
                                }
                              },
                              child: Container(
                                padding: EdgeInsets.symmetric(horizontal: 12, vertical: 8),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(8)
                                ),
                                child: Wrap(
                                  alignment: WrapAlignment.center,
                                  crossAxisAlignment: WrapCrossAlignment.center,
                                  children: [
                                    Icon(Icons.call_made_outlined, size: 22, color: Colors.white,),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 4.0),
                                      child: Text("Transfer", style: textStyleTitle(13),),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Lịch sửa giao dịch", style: textStyleTitleCustom(15),),
                          if (controller.listTransaction.isEmpty) Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text("Bạn chưa có giao dịch nào", style: textStyleNormalCustom(14),)
                              ],
                            ),
                          ) else
                            ListView.builder(
                              padding: EdgeInsets.zero,
                              itemCount: controller.listTransaction.length,
                              scrollDirection: Axis.vertical,
                              shrinkWrap: true,
                              physics: const BouncingScrollPhysics(),
                              // physics: AlwaysScrollableScrollPhysics(),
                              itemBuilder: (context, index){
                                Result result = controller.listTransaction[index];
                                bool isDebit = false;
                                String title = '';
                                Color color = MyColor.content;
                                String from;
                                if(result.from.toLowerCase() == controller.account.value.toLowerCase()){
                                  title = "Chuyển tiền";
                                  color = MyColor.red2D;
                                  isDebit = true;
                                  from = result.to;
                                }else{
                                  title = "Nhận tiền";
                                  color = MyColor.green38;
                                  isDebit = false;
                                  from = result.from;
                                }
                                return GestureDetector(
                                  onTap: (){
                                    controller.showDialogNormal(
                                        title: title,
                                        colorTitle: color,
                                        content: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.only(top: 16.0),
                                              child: RichText(
                                                textAlign: TextAlign.start,
                                                text: TextSpan(
                                                    children: [
                                                      TextSpan(
                                                          text: "BlockNumber: ",
                                                          style: textStyleTitle(15)
                                                      ),
                                                      TextSpan(
                                                          text: result.blockNumber,
                                                          style: textStyleNormalCustom(15)
                                                      ),
                                                    ]
                                                ),
                                              ),
                                              // child: Text("BlockNumber: ${result.blockNumber}", style: textStyleNormalCustom(14),),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(top: 8.0),
                                              child: RichText(
                                                textAlign: TextAlign.start,
                                                text: TextSpan(
                                                    children: [
                                                      TextSpan(
                                                          text: "None: ",
                                                          style: textStyleTitle(15)
                                                      ),
                                                      TextSpan(
                                                          text: result.nonce,
                                                          style: textStyleNormalCustom(15)
                                                      ),
                                                    ]
                                                ),
                                              ),
                                            ),
                                            // Padding(
                                            //   padding: const EdgeInsets.only(top: 8.0),
                                            //   child: RichText(
                                            //     textAlign: TextAlign.start,
                                            //     text: TextSpan(
                                            //         children: [
                                            //           TextSpan(
                                            //               text: "From: ",
                                            //               style: textStyleTitle(15)
                                            //           ),
                                            //           TextSpan(
                                            //               text: result.from,
                                            //               style: textStyleNormalCustom(15)
                                            //           ),
                                            //         ]
                                            //     ),
                                            //   ),
                                            // ),
                                            Padding(
                                              padding: const EdgeInsets.only(top: 8.0),
                                              child: from.startsWith("0x0000000")?
                                              RichText(
                                                textAlign: TextAlign.start,
                                                text: TextSpan(
                                                    children: [
                                                      TextSpan(
                                                          text: isDebit?"Tới contract: ":"Từ contract: ",
                                                          style: textStyleTitle(15)
                                                      ),
                                                      TextSpan(
                                                          text: result.contractAddress,
                                                          style: textStyleNormalCustom(15, color: color)
                                                      ),
                                                    ]
                                                ),
                                              )
                                                  :RichText(
                                                textAlign: TextAlign.start,
                                                text: TextSpan(
                                                    children: [
                                                      TextSpan(
                                                          text: isDebit?"Tới địa chỉ: ":"Từ địa chỉ: ",
                                                          style: textStyleTitle(15)
                                                      ),
                                                      TextSpan(
                                                          text: from,
                                                          style: textStyleNormalCustom(15, color: color)
                                                      ),
                                                    ]
                                                ),
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(top: 8.0),
                                              child: RichText(
                                                textAlign: TextAlign.start,
                                                text: TextSpan(
                                                    children: [
                                                      TextSpan(
                                                          text: "Số tiền: ",
                                                          style: textStyleTitle(15)
                                                      ),
                                                      TextSpan(
                                                          text: result.value+"Bsc",
                                                          style: textStyleNormalCustom(15, color: color)
                                                      ),
                                                    ]
                                                ),
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(top: 8.0),
                                              child: RichText(
                                                textAlign: TextAlign.start,
                                                text: TextSpan(
                                                    children: [
                                                      TextSpan(
                                                          text: "GasUse: ",
                                                          style: textStyleTitle(15)
                                                      ),
                                                      TextSpan(
                                                          text: result.gasUsed,
                                                          style: textStyleNormalCustom(15)
                                                      ),
                                                    ]
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                        textOk: "Ok",
                                        onClickOke: (){}
                                    );
                                  },
                                  child: Container(
                                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                                    decoration: const BoxDecoration(
                                      border: Border(
                                        bottom: BorderSide(width: 1, color: MyColor.border),
                                      ),
                                    ),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Expanded(
                                          flex: 2,
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,

                                            children: [
                                              Padding(
                                                padding: const EdgeInsets.only(bottom: 8.0),
                                                child: Text(title, style: textStyleTitleCustom(14, color: color),textAlign: TextAlign.start),
                                              ),
                                              Text(controller.convertTime(result.timeStamp), style: textStyleNormalCustom(13),textAlign: TextAlign.start),
                                            ],
                                          ),
                                        ),
                                        Expanded(
                                          flex: 2,
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.end,
                                            children: [
                                              Padding(
                                                padding: const EdgeInsets.only(bottom: 8.0),
                                                child: Text((isDebit?"-":"+")+"${result.value}BSC", style: textStyleTitleCustom(14, color: isDebit? MyColor.red2D: MyColor.green38),textAlign: TextAlign.start),
                                              ),
                                              Text(isDebit?"Bạn đã chuyển tiền tới địa chỉ ${result.to}":"Bạn đã nhận tiền từ địa chỉ ${result.from}", style: textStyleNormalCustom(13, color: MyColor.content),textAlign: TextAlign.end, overflow: TextOverflow.ellipsis,),
                                            ],
                                          ),
                                        ),

                                      ],
                                    ),
                                  ),
                                );
                              },
                            )
                          // Padding(
                          //   padding: const EdgeInsets.symmetric(vertical: 0.0),
                          //   child: ,
                          // )
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Positioned(
                top: 0,
                left: 0,
                right: 0,
                child: Container(
                  padding: EdgeInsets.only(top: 30, bottom: 8),
                  decoration: BoxDecoration(
                      gradient: MyColor.gradientPrimary
                  ),
                  child: Stack(
                    children: [
                      // IconButton(
                      //   icon: Icon(Icons.arrow_back_ios_outlined, color: Colors.white),
                      //   onPressed: () => Get.back(),
                      // ),
                      Container(
                          height: 50,
                          alignment: Alignment.center,
                          child: Padding(
                            padding: const EdgeInsets.only(top: 14.0),
                            child: SlideTransition(
                                position: controller.opacityTween,
                                child: Padding(
                                  padding: const EdgeInsets.only(bottom: 8.0),
                                  child: Text("Ví của tôi", style: textStyleTitle(17), textAlign: TextAlign.center,),
                                )),
                          )
                      ),
                      Positioned(
                          right: 16,
                          top: 8,
                          bottom: 0,
                          child: ExpandedSection(
                            child: Row(
                              children: [
                                Wrap(
                                  children: [
                                    SizedBox(
                                      width: 50,
                                      height: 40,
                                      child: GestureDetector(
                                        onTap: (){
                                          if(!controller.isTransfer) {
                                            Get.toNamed(RouterName.transfer, arguments: {
                                              'from': "Bsc",
                                              'address': controller.account.value
                                            });
                                          }
                                        },
                                        child: Container(
                                          padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
                                          decoration: BoxDecoration(
                                            color: MyColor.red2D,
                                            borderRadius: BorderRadius.circular(12),
                                          ),
                                          child: Wrap(
                                            alignment: WrapAlignment.center,
                                            crossAxisAlignment: WrapCrossAlignment.center,
                                            children: const [
                                              Icon(Icons.call_made_outlined, size: 22, color: Colors.white,),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                InkWell(
                                  onTap: (){
                                    controller.logout();
                                  },
                                  child: Container(
                                    margin: EdgeInsets.only(left: 12),
                                    height: 40,
                                    width: 50,
                                    padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.circular(12)
                                    ),
                                    child: Icon(Icons.logout, color: MyColor.gray8C, size: 25),
                                  ),
                                )
                              ],
                            ),
                            expand: !controller.firstAnimation.value,
                          )

                      ),
                      Positioned(
                        top: 8,
                        right: 16,
                        child: InkWell(
                          onTap: (){
                            controller.logout();
                          },
                          child: Container(
                            height: 40,
                            width: 50,
                            margin: EdgeInsets.only(left: 12),
                            padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(12)
                            ),
                            child: Icon(Icons.logout, color: MyColor.gray8C, size: 25),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
