
import 'package:dio/dio.dart';

import '../../../base/base_repository.dart';
import '../../../model/transaction_response.dart';


class BscWalletRepository extends BaseRepository{
  Future getListTransaction(String url, int page, String address) async {
    TransactionResponse transactionResponse;
    try {
      Response response = await dio.get(url,
          queryParameters: {"module": "account", "action": "tokentx",
            "address": address, "startblock": 0, "endblock": 99999999, "page": page, "offset": 15, "sort": "desc"});
      transactionResponse = TransactionResponse.fromJson(response.data);
    } on DioError catch (e) {
      print(e);
    }
    return transactionResponse;
  }
}