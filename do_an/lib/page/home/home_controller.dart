import 'package:flutter/material.dart';
import 'package:flutter_do_an/base/base_view_view_model.dart';
import 'package:flutter_do_an/utils/wallet_connect_eth_credentials.dart';
import 'package:walletconnect_dart/walletconnect_dart.dart';
import 'package:web3dart/web3dart.dart';
import 'bscWallet/bsc_wallet.dart';
import 'ethWallet/eth_wallet.dart';
import 'home_repository.dart';

class HomeController extends BaseController<HomeRepository>{
  Web3Client ethClient;
  Web3Client bscClient;
  var isLoading = false.obs;
  var myData;
  num money  = 0;
  var transaction = ''.obs;
  var account = "".obs;
  WalletConnectEthereumCredentials credentials;
  String uriOpen = '';
  WalletConnect connector;
  var index = 0.obs;
  List<Widget> widgetOptions = <Widget>[
    const ETHWalletPage(),
    const BSCWalletPage(),
  ];
  @override
  void onInit() async{
    // TODO: implement onInit
    super.onInit();
  }
}