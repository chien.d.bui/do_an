import 'dart:convert';
import 'dart:typed_data';

import 'package:eth_sig_util/eth_sig_util.dart';
import 'package:event_bus/event_bus.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:flutter_do_an/base/base_view_view_model.dart';
import 'package:flutter_do_an/event/transfer_event.dart';
import 'package:flutter_do_an/model/transaction_response.dart';
import 'package:flutter_do_an/utils/storage_utils.dart';
import 'package:flutter_do_an/utils/wallet_connect_eth_credentials.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:walletconnect_dart/walletconnect_dart.dart';
import 'package:web3dart/web3dart.dart';
import 'package:web3dart/contracts/erc20.dart';
import 'package:http/http.dart';

import '../../../base/api_constant.dart';
import '../../../model/info_login.dart';
import '../../../routes/router_name.dart';
import 'eth_wallet_repository.dart';

class ETHWalletController extends BaseController<EthWalletRepository> with GetSingleTickerProviderStateMixin{
  Web3Client ethClient;
  Web3Client bscClient;
  var isLoading = false.obs;
  var myData;
  var transaction = ''.obs;
  var account = "".obs;
  Credentials credentials;
  String uriOpen = '';
  WalletConnect connector;
  var index = 0.obs;
  String addressTokenEth = "0x9A3cCE889Cd96fb8321E33BEa8796fE5BAB65916";
  String addressBridgeEth = "0xD68bDa1E7Afe792b7857bC30E0620eA8f4A97f19";
  final _eventBus = Get.find<EventBus>();
  ContractEvent transferEventEth;
  List<String> step = ["Burn", "Mint"];
  TextEditingController ethCtl = TextEditingController();
  String privateKey = "";
  RoundedLoadingButtonController buttonController = RoundedLoadingButtonController();
  RoundedLoadingButtonController buttonTopController = RoundedLoadingButtonController();
  BigInt nonce;
  ContractEvent transferEvent;
  Uint8List signature;
  bool isTransfer = false;
  Erc20 erc20Eth;
  RxList<Result> listTransaction = <Result>[].obs;
  AnimationController animationController;
  Animation opacityTween;
  Animation sizeTween;
  final scrollController = ScrollController();
  var firstAnimation = true.obs;
  var pageKey = 1;
  // var isLoadingMore = false.obs;
  bool isMax = false;
  @override
  void onInit() async{
    // TODO: implement onInit
    super.onInit();
    animationController = AnimationController(vsync: this, duration: Duration(milliseconds: 500));
    opacityTween = Tween<Offset>(begin: Offset(0, 0), end: Offset(-1.5, 0)).animate(CurvedAnimation(
        parent: animationController,
        curve: Curves.easeOutBack,
        reverseCurve: Curves.easeInBack
    ));
    sizeTween = CurvedAnimation(
      parent: animationController,
      curve: Curves.fastOutSlowIn,
    );
    scrollController.addListener(() {_onScrolling();});

    InfoLogin infoLogin = StorageUtils.getInfoLogin();
    account.value = infoLogin.address;
    privateKey = infoLogin.privateKey;
    ethClient = Web3Client("https://kovan.infura.io/v3/f4e05aef1661452d915d34547435604d", Client());
    credentials = EthPrivateKey.fromHex(privateKey);
    // await walletConnectEth();
    erc20Eth = Erc20(address: EthereumAddress.fromHex(addressTokenEth), client: ethClient);
    getBalanceEth();
    getListTransaction();
    erc20Eth.transferEvents(fromBlock: const BlockNum.exact(1), toBlock: const BlockNum.exact(99999999)).take(1).listen((event) {
      print('${event.from} sent ${event.value} MetaCoins to ${event.to}!');
      if(event.from.hex.startsWith("0x0000000")|| event.to.hex.startsWith("0x0000000")){
        return;
      }
      showSuccessSnackBar(message: "Chuyển tiền thành công");
      getBalanceEth();
      isTransfer = false;
      buttonController.reset();
    });
    _eventBus.on<TransferEvent>().listen((event) async{
      if(event.token == "ETH") {
        nonce = event.nonce;
        signature = event.signature;
        transferCoinEth(event.from, event.to, event.value, "mint");
      }
    });
    // hash();
  }
  Future getBalanceEth() async {
    isLoading.value = true;
    BigInt result = await erc20Eth.balanceOf(EthereumAddress.fromHex(account.value));
    myData = result;
    isLoading.value = false;
  }
  void _onScrolling() {
    var offset = scrollController.offset;
    if(offset > 120 && firstAnimation.value){
      firstAnimation.value = false;
      animationController.forward();
    }
    if(offset < 120 && !firstAnimation.value){
      firstAnimation.value = true;
      animationController.reverse();
    }
    // if(offset > scrollController.position.maxScrollExtent-50 && !isLoadingMore.value){
    //   if(!isMax){
    //     isLoadingMore.value = true;
    //     pageKey++;
    //     getListTransaction(false);
    //   }
    // }
  }
  Future<void> getListTransaction() async {
    TransactionResponse transactionResponse = await api.getListTransaction(ApiConst.apiKovan, 1, account.value);
    if(transactionResponse == null){
      showAlert(middleText: "Đã có lỗi xảy ra, vui lòng thử lại");
      return;
    }
    if(transactionResponse.status == "1") {
      // if(isRenew) {
        listTransaction.value = [];
      // }
      if(transactionResponse.result.isNotEmpty) {
        listTransaction.addAll(transactionResponse.result);
      }
      listTransaction.refresh();
      // isLoadingMore.value = false;
      return;
    } else {
      // isLoadingMore.value = false;
      // showAlert(middleText: transactionResponse.message);
      return;
    }
  }
  Future transfer(EthereumAddress to, BigInt value) async {
    String result;
    result = await erc20Eth.transfer(to, value, credentials: credentials);
    print(result);
  }
  Future<DeployedContract> loadBridgeEth() async{
    String abiStringFile = await rootBundle.loadString("assets/abi/abiBridgeEth.json");
    final contract = DeployedContract(ContractAbi.fromJson(abiStringFile, "BridgeEth"), EthereumAddress.fromHex(addressBridgeEth));
    transferEventEth = contract.event('Transfer');
    return contract;
  }
  void logout(){
    StorageUtils.isLogin(false);
    Get.offNamed(RouterName.login);
  }
  walletConnectEth() async {
    connector = WalletConnect(
      uri: uriOpen,
      bridge: 'https://bridge.walletconnect.org',
      clientMeta: const PeerMeta(
        name: 'MCCoin Connect',
        description: 'MCCoin Developer App',
        url: 'https://myceleb.vn',
        icons: [
          'https://static9.depositphotos.com/1230058/1153/v/600/depositphotos_11534292-stock-illustration-golden-coin.jpg'
        ],
      ),
    );
    // Subscribe to events
    connector.on('connect', (session){ print(session);
      if (account.value.isNotEmpty) {
        print("Success: "+account.value);
        EthereumWalletConnectProvider provider =
        EthereumWalletConnectProvider(connector);
        credentials = WalletConnectEthereumCredentials(provider: provider);
      }
    });
    connector.on('session_update', (payload) => print(payload));
    connector.on('disconnect', (session) => print(session));
    // Create a new session
    if (!connector.connected) {
      if(account.value.isEmpty) {
        SessionStatus session = await connector.createSession(
            chainId: 42,
            onDisplayUri: (uri) async =>
            {
              uriOpen = uri,
              await launch(uri).catchError((e) {
                launch(
                    "https://play.google.com/store/apps/details?id=io.metamask");
              })});
        account.value = session.accounts[0];
        StorageUtils.setUriMTM(uriOpen);
        StorageUtils.setAccessToken(account.value);
        getBalanceEth();
      }else{
        await connector.approveSession(chainId: 42, accounts: [account.value]);
      }
    }
  }
  Future<String>  submitEth(String functionName, List<dynamic> args) async{
    DeployedContract contract = await loadBridgeEth();
    final ethFunction = contract.function(functionName);
    String result;
    // await launch(uriOpen).whenComplete(() async{
    print("Send.....");
    result = await ethClient.sendTransaction(
        credentials,
        Transaction.callContract(
          from: EthereumAddress.fromHex(account.value),
          contract: contract,
          function: ethFunction, parameters: args,
          gasPrice: EtherAmount.fromUnitAndValue(EtherUnit.kwei, 2000000),
          maxGas: 100000,
          value: EtherAmount.fromUnitAndValue(EtherUnit.finney, 0),
          nonce: await ethClient.getTransactionCount(EthereumAddress.fromHex(account.value), atBlock: BlockNum.pending()),
        ), fetchChainIdFromNetworkId: false, chainId: 42
      ).catchError((e){
        print(e);
        showErrorSnackBar(message: "Đã có lỗi xảy ra: "+e.toString());
      // });
    });
    ethClient
        .events(FilterOptions.events(contract: contract, event: transferEventEth))
        .take(1)
        .listen((event) async {
      final decoded = transferEventEth.decodeResults(event.topics, event.data);
      final from = decoded[0] as EthereumAddress;
      final to = decoded[1] as EthereumAddress;
      final value = decoded[2] as BigInt;
      final time = decoded[3];
      final nonce = decoded[4] as BigInt;
      final signature = decoded[5] as Uint8List;
      var indexStep = decoded[6];
      if (kDebugMode) {
        print("Event nè... $from... $to... $value... $nonce... $indexStep");
      }
      ethCtl.text = "";
      isTransfer = false;
      buttonController.reset();
      getBalanceEth();
      getListTransaction();
      if(indexStep.toString() == "0") {
        showSuccessSnackBar(message: "Chuyển tiền thành công");
        _eventBus.fire(TransferEvent(to: to, value: value, token: "BSC", nonce:  nonce, signature: signature, from: from));
      }
    });
    return result;
  }
  Future<String> transferCoinEth(EthereumAddress from, EthereumAddress to, var value, String step) async{
    List arg;
    if(step == "burn"){
      List<String> types = ["address", "address", "uint256", "uint256"];
      nonce = BigInt.from(await ethClient.getTransactionCount(EthereumAddress.fromHex(account.value), atBlock: BlockNum.pending()));
      final messageHash = AbiUtil.soliditySHA3(types,
          [from.addressBytes,
            to.addressBytes, value, nonce]);
      print("data nè... $from... $to... $value... $nonce...");
      signature = await credentials.signPersonalMessage(messageHash);
      arg = [to, value, nonce, signature];
    }else{
      arg = [from, to, value, nonce, signature];
    }
    var response = await submitEth(step, arg);
    return response;
  }
}