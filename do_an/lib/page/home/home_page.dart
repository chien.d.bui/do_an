
import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_do_an/base/base_view_view_model.dart';
import 'package:flutter_do_an/res/my_config.dart';
import 'package:google_nav_bar/google_nav_bar.dart';

import 'bscWallet/bsc_wallet_controller.dart';
import 'home_controller.dart';

class HomePage extends BaseViewModel<HomeController> {

  @override
  Widget vmBuilder(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColor.background,
      resizeToAvoidBottomInset: false,
      body: Container(
        child: PageTransitionSwitcher(
            reverse: false,
            duration: Duration(milliseconds: 500),
            transitionBuilder: (child, animation, secondaryAnimation)=>
                SharedAxisTransition(
                    fillColor: MyColor.background,
                    child: child,
                    animation: animation,
                    secondaryAnimation: secondaryAnimation,
                    transitionType: SharedAxisTransitionType.horizontal),
            child: controller.widgetOptions.elementAt(controller.index.value)
        ),
        // child: controller.widgetOptions.elementAt(controller.index.value)
      ),
      bottomNavigationBar: GNav(
        rippleColor: Colors.grey[300],
        hoverColor: Colors.grey[100],
        gap: 8,
        // activeColor: Colors.red,
        iconSize: 24,
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 12),
        tabMargin: EdgeInsets.only(bottom: 16,top: 8),
        duration: Duration(milliseconds: 400),
        tabBackgroundGradient: MyColor.gradientPrimary,
        color: Colors.white,
        activeColor: Colors.white,
        // color: Colors.red,
        tabs: [
          GButton(
            leading: Image.asset("assets/image/ethereum.png", width: 24, color: Colors.white,),
            text: 'ETHWallet',
          ),
          GButton(
            leading: Image.asset("assets/image/binance.png", width: 24, color: Colors.white,),
            text: 'BSCWallet',
          ),
        ],
        selectedIndex: controller.index.value,
        onTabChange: (index) {
          // if(controller.index.value > index) controller.reverse.value = true;
          // else controller.reverse.value = false;
          controller.index.value = index;
          switch(index){
            case 1:{
              break;
            }case 2:{
            break;
          }
          }
        },
      ),
    );
  }

}
