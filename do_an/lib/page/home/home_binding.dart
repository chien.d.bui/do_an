
import 'package:flutter_do_an/page/home/ethWallet/eth_wallet_repository.dart';
import 'package:get/get.dart';

import 'bscWallet/bsc_wallet_controller.dart';
import 'bscWallet/bsc_wallet_repository.dart';
import 'ethWallet/eth_wallet_controller.dart';
import 'home_controller.dart';
import 'home_repository.dart';

class HomeBinding extends Bindings{
  @override
  void dependencies() {
    // TODO: implement dependencies
    Get.put(HomeRepository());
    Get.put(HomeController());
    Get.put(BscWalletRepository());
    Get.put(BSCWalletController());
    Get.put(EthWalletRepository());
    Get.put(ETHWalletController());
  }

}