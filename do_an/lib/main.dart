import 'dart:convert';

import 'package:event_bus/event_bus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_do_an/res/my_config.dart';
import 'package:flutter_do_an/res/my_translations.dart';
import 'package:flutter_do_an/routes/pages.dart';
import 'package:flutter_do_an/routes/router_name.dart';
import 'package:flutter_do_an/utils/storage_utils.dart';
import 'package:flutter_flavor/flutter_flavor.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import 'base/myceleb_api.dart';
bool isLogin = false;
void main() async{
  FlavorConfig(
      name: "DEVELOPER",
      variables: {
        "appScheme": "vn.myceleb.app",
        // "baseCelebUrl": "http://172.168.24.43:3000/",
        "baseCelebUrl": "https://api.myceleb.vn/",
      }
  );
  await GetStorage.init();
  StorageUtils.storage;
  MyCelebApi.dio;
  MyTranslations.init();
  Get.put(EventBus());
  if(StorageUtils.getLoginStatus() != null ){
    isLogin = StorageUtils.getLoginStatus();
  }
  runApp(const MyApp());
}
class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      // builder: EasyLoading.init(),
      initialRoute:isLogin? RouterName.home: RouterName.login,
      locale: MyTranslations.locale,
      fallbackLocale: MyTranslations.fallbackLocale,
      translations: MyTranslations(),
      theme: ThemeData(
        primarySwatch: Colors.deepOrange,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        unselectedWidgetColor: MyColor.content
      ),
      getPages: Pages.pages()
    );
  }
}
