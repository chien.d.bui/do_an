import 'package:flutter/material.dart';
import 'package:flutter_do_an/widgets/widget_utils.dart';
import 'base_controller.dart';
export 'base_controller.dart';

/// created by Bui Duc Chien
/// 04/08/21

abstract class BaseViewModel<T extends BaseController> extends StatelessWidget with WidgetUtils{
  const BaseViewModel({Key key}) : super(key: key);

  final String tag = null;

  T get controller => GetInstance().find<T>(tag: tag);

  @override
  Widget build(BuildContext context){
    return Obx(()=>vmBuilder(context));
  }

  Widget vmBuilder(BuildContext context);
}

abstract class BaseView<T extends BaseController> extends StatelessWidget with WidgetUtils {

  const BaseView({Key key}) : super(key: key);
  final String tag = null;
  T get controller => GetInstance().find<T>(tag: tag);
  @override
  Widget build(BuildContext context) {
    return GetBuilder<T>(
      builder: (controller){
        return vBuilder(context);
      },
    );
  }

  Widget vBuilder(BuildContext context);
}