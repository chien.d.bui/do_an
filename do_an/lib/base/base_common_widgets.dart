import 'package:flutter_do_an/res/my_config.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'base_controller.dart';

/// created by Bui Duc Chien
/// 04/08/21

class BaseCommonWidgets  {

  void showSnackBar({String title = "Alert", String message = ""}) {
    Get.snackbar(title, message,
      colorText: MyColor.text,
      backgroundColor: Colors.white,
      barBlur: 8.0,
      snackPosition: SnackPosition.BOTTOM,
      margin: EdgeInsets.only(bottom: 10.0, left: 10.0, right: 10.0),
      duration: Duration(seconds: 2),
    );
  }

  void showErrorSnackBar({String title = "Alert", String message = ""}) {
    Get.snackbar(title, message,
        colorText: Colors.white,
        backgroundColor: Color(0x8AD32F2F),
        barBlur: 10.0,
        snackPosition: SnackPosition.BOTTOM,
        margin: EdgeInsets.only(bottom: 10.0, left: 10.0, right: 10.0),
        duration: Duration(seconds: 2),
        icon: Icon(Icons.error, color: Colors.white)
    );
  }

  void showSuccessSnackBar({String title = "Thông báo", String message = ""}) {
    Get.snackbar(title, message,
        backgroundColor: Color(0x8A2E7D32),
        barBlur: 10.0,
        snackPosition: SnackPosition.BOTTOM,
        margin: EdgeInsets.only(bottom: 10.0, left: 10.0, right: 10.0),
        duration: Duration(seconds: 2),
        icon: Icon(Icons.check_circle,color: Colors.white,),
        colorText: Colors.white,

    );
  }

  void showAlert({
    String middleText,
    String textBtn = "Đồng ý"
  }) {
    Alert(
      closeIcon: Container(),
      context: Get.context,
      style: AlertStyle(
        backgroundColor: MyColor.background,
        titleStyle: TextStyle(fontSize: 17,color: Colors.white,fontWeight: FontWeight.bold),
        descStyle: TextStyle(fontSize: 13,color: MyColor.content),
        descTextAlign: TextAlign.center,
      ),
      title: "Thông báo",
      content: Padding(
          padding: EdgeInsets.only(top: 16),
          child: Text(middleText,style: TextStyle(fontSize: 13,color: MyColor.content),textAlign: TextAlign.center,)
      ),
      buttons: [
        DialogButton(
          height: 30,
          width: 80,
          radius: BorderRadius.circular(12),
          gradient: MyColor.gradientPrimary,
          child: Text(
            textBtn,
            style: TextStyle(color: Colors.white, fontSize: 14),
          ),
          onPressed: () =>Get.back(),
        ),
      ],
    ).show();
  }
  void showDialogNormal({
    String title,
    String desc,
    Widget content,
    void onClickOke(),
    Color colorTitle,
    String textCancel = "Huỷ",
    String textOk = "Tiếp tục",
  }) {
    Alert(
      closeIcon: Container(),
      context: Get.context,
      style: AlertStyle(
        backgroundColor: MyColor.background,
        titleStyle: TextStyle(fontSize: 17,color: colorTitle,fontWeight: FontWeight.bold),
        descStyle: const TextStyle(fontSize: 13,color: MyColor.content),
        descTextAlign: TextAlign.center,
      ),
      title: title,
      desc: desc,
      content: Container(child: content),
      buttons: [
        // DialogButton(
        //   height: 30,
        //   radius: BorderRadius.circular(12),
        //   child: Text(
        //     textCancel,
        //     style: TextStyle(color: Colors.white, fontSize: 14),
        //   ),
        //   onPressed: () => {
        //     Get.back(),
        //   },
        //   color: Colors.white.withOpacity(0.1),
        // ),
        DialogButton(
          height: 30,
          radius: BorderRadius.circular(12),
          gradient: MyColor.gradientPrimary,
          child: Text(
            textOk,
            style: TextStyle(color: Colors.white, fontSize: 14),
          ),
          onPressed: () => {Get.back(), onClickOke()},
        ),
      ],
    ).show();
  }
}
