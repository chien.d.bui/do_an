
import 'package:flutter_flavor/flutter_flavor.dart';

class ApiConst{
  static final String baseMyCelebUrl = FlavorConfig.instance.variables["baseCelebUrl"];
  static final String baseImageMyCeleb = "https://celeb-videos.hn.ss.bfcplatform.vn";

  //api celeb
  static const String apiKovan = "https://api-kovan.etherscan.io/api";
  static const String apiBSC = "https://api-testnet.bscscan.com/api";

}