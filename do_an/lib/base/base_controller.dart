import 'package:flutter_do_an/utils/storage_utils.dart';
import 'package:flutter_do_an/utils/utilities.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import 'base_common_widgets.dart';
import 'base_repository.dart';
export 'package:get/get.dart';
/// created by Bui Duc Chien
///  04/08/21

class BaseController<T extends BaseRepository> extends GetxController
    with BaseCommonWidgets,Utilities {
  // LoadingPage loadingPage = LoadingPage.loading;
  GetStorage storage = StorageUtils.storage;
  T get api => Get.find<T>();
  @override
  void onInit() {
    super.onInit();
  }

  void onRefresh() {}
  
  void onLoadMore() {}


}
