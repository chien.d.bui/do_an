
import 'dart:convert';
import 'dart:io';
import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_do_an/utils/storage_utils.dart';
import 'package:flutter_flavor/flutter_flavor.dart';

import 'api_constant.dart';
class MyCelebApi{
  static Dio _dio;
  static Dio get dio{
    if(_dio == null){
      _dio = new Dio();
      _dio.interceptors.add(MyCelebInterceptors());
      (_dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
          (HttpClient dioClient) {
        dioClient.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
        return dioClient;
      };
    }
    return _dio;
  }
}


class MyCelebInterceptors extends InterceptorsWrapper {
  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    // options.baseUrl = ApiConst.baseMyCelebUrl;
    String language = StorageUtils.getLanguage();
    String accessToken = StorageUtils.getAccessToken();
    if(language!=null && language.isNotEmpty){
      options.headers['Accept-Language'] = language;
    }
    if(accessToken!=null && accessToken.isNotEmpty){
      options.headers['Authorization'] = "Bearer $accessToken";
    }
    String environment = FlavorConfig.instance.name;
    if(environment != null && environment == 'DEVELOPER'){
      debugPrint("<<<<<-----ApiInterceptor-options--START->>>>");
      debugPrint("${options.baseUrl}");
      debugPrint("${options.path}");
      debugPrint("${options.headers}");
      debugPrint("${options.queryParameters}");
      // debugPrint(jsonEncode(options.data));
      debugPrint("${options.data}");
      debugPrint("<<<<<-----ApiInterceptor-options--END->>>>");
    }
    return super.onRequest(options, handler);
  }
  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    String environment = FlavorConfig.instance.name;
    if(environment != null && environment == 'DEVELOPER'){
      debugPrint("<<<<<-----ApiInterceptor-response--START->>>>");
      debugPrint("${response.statusCode}");
      debugPrint("${response.headers}");
      debugPrint(jsonEncode(response.data));
      debugPrint("${response.statusMessage}");
      debugPrint("<<<<<-----ApiInterceptor-response--END->>>>");
    }
    return super.onResponse(response, handler);
  }
  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    String environment = FlavorConfig.instance.name;
    if(environment != null && environment == 'DEVELOPER'){
      debugPrint("<<<<<-----ApiInterceptor-err--START->>>>");
      // debugPrint("${err.response?.statusCode}");
      debugPrint("${err.response}");
      debugPrint("${err.message}");
      debugPrint("<<<<<-----ApiInterceptor-err--END->>>>");

    }
    return super.onError(err, handler);
  }

}