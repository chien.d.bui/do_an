module.exports = {
  networks: {
    development: {
      host: "172.168.24.167",
      port: 7545,
      network_id: "*", // Match any network id
    },
    advanced: {
      websockets: true, // Enable EventEmitter interface for web3 (default: false)
    },
  },
  contracts_build_directory: "./src/abis/",
  compilers: {
    solc: {
      version: "^0.8.0"
    },
  },
};