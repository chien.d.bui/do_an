const ImperialCredits = artifacts.require("ImperialCredits");
const MainBridge = artifacts.require("MainBridge");
const SideBridge = artifacts.require("SideBridge");
const TokenChild = artifacts.require("TokenChild");
module.exports = async function (deployer, network, addresses) {
   await imperialCredits = ImperialCredits.deploy(1000);
   await mainBridge = MainBridge.deploy(imperialCredits.address, addresses[0]);
   await sideBridge = SideBridge.deploy(mainBridge.address);
   await tokenChild = TokenChild.deploy(sideBridge.address);
};
